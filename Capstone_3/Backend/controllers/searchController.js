const Product = require('../models/product');
const Cart = require('../models/cart');

// #############################################################################

// Search products
exports.searchProducts = async (req, res) => {
  const { keyword } = req.query;

  try {
    // Perform a case-insensitive search for products containing the keyword in their title or description
    const products = await Product.find({
      $or: [
        { title: { $regex: keyword, $options: 'i' } },
        { description: { $regex: keyword, $options: 'i' } }
      ]
    });

    res.json(products);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while searching for products.' });
  }
};

// #############################################################################


// Search products by category
exports.searchByCategory = (req, res) => {
  const { category } = req.params;

  Product.find({ category })
    .then(products => {
      res.json(products);
    })
    .catch(error => {
      res.status(500).json({ error: 'Failed to search products by category' });
    });
};




// #############################################################################

// Search products by price range
exports.searchByPriceRange = (req, res) => {
  const { minPrice, maxPrice } = req.query;

  Product.find({ price: { $gte: minPrice, $lte: maxPrice } })
    .then(products => {
      res.json(products);
    })
    .catch(error => {
      res.status(500).json({ error: 'Failed to search products by price range' });
    });
};

