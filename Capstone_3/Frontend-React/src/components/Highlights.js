import { Row, Col, Card } from "react-bootstrap";
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import "./components.css";


const MAX_LENGTH = 100;

const ShortenedText = ({ text, link }) => {
  const [isExpanded, setIsExpanded] = useState(false);

  const toggleExpansion = () => {
    setIsExpanded(!isExpanded);
  };

  const renderShortenedText = () => {
    return (
      <>
        {text.length <= MAX_LENGTH ? (
          text
        ) : (
          <>
            {text.slice(0, MAX_LENGTH)}...
            <Link to={link} className="text-primary ml-1 readMore">
              See more
            </Link>
          </>
        )}
      </>
    );
  };

  const renderFullText = () => {
    return (
      <>
        {text}
        <span onClick={toggleExpansion} className="text-primary ml-1 cursor-pointer">
          See less
        </span>
      </>
    );
  };

  return <>{isExpanded ? renderFullText() : renderShortenedText()}</>;
};

export default function Hightlights(){
  return(
    <Row className="my-5">
      {/*First Card*/}
      <Col xs={12} md={4} className="card-border">
        <Card className="cardHighlight p-3 flex-grow-1">
        <div className="image-container cardImageContainer">
          <Card.Img variant="top" className="cardImage" src="https://th.bing.com/th/id/OIP.AFqWyfCfR_HQJFw9LNES3QHaJC?pid=ImgDet&rs=1" />
            <div className="cardOverlay">
                <span className="cardOverlayText" id="card-title1">Interview</span>
            </div>
          </div>

          <Card.Body>
            <Card.Title className="cardTitle">Quill Guidance: A Candid Conversation with Gina Apostol on Empowering Emerging Writers</Card.Title>
            <Card.Text className="cardText">
              <ShortenedText text="How did you become a writer? I began writing a novel, eventually called Bibliolepsy, when I was around nineteen, and my decision to work on a novel, I think, was my beginning as a writer." 

                    link="https://advicetowriters.com/interviews/1sz580ipw28i3cw09y2tdir0aao5ww" />
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>

      {/*Second Card*/}
      <Col xs={12} md={4} className="card-border">
        <Card className="cardHighlight p-3 d-flex flex-grow-1">
        <div className="image-container cardImageContainer">
          <Card.Img variant="top" className="cardImage" src="https://thenextsomewhere.com/wp-content/uploads/2021/04/books-filipinaauthors-pin.jpg" />
            <div className="cardOverlay" id="card-title2">
                <span className="cardOverlayText" >List</span>
            </div>
          </div>

          <Card.Body>
            <Card.Title className="cardTitle">15 Books by Female Filipino Authors That Demand to be Read</Card.Title>
            <Card.Text className="cardText">
              <ShortenedText text="In this enlightening article, we delve into the vibrant world of Gen Z literature, celebrating the exceptional talent of female Filipino authors. Curated with care, our selection features 15 thought-provoking books that resonate deeply with the Gen Z experience. From mesmerizing novels to empowering memoirs, these literary works demand to be devoured by young minds seeking diverse narratives and fresh perspectives. Join us on this literary odyssey that amplifies the voices of these remarkable authors and ignites the passion for reading among the Gen Z generation." 

                    link="https://thenextsomewhere.com/2021/04/12/16-books-filipina-authors-reading-list/" />
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>

      {/*Third Card*/}
      <Col xs={12} md={4} className="card-border">
        <Card className="cardHighlight p-3 flex-grow-1">
        <div className="image-container cardImageContainer">
          <Card.Img variant="top" className="cardImage" src="https://www.scoutmag.ph/wp-content/uploads/2018/08/SCOUT-20-cover13.jpg" />
            <div className="cardOverlay" id="card-title3">
                <span className="cardOverlayText" >Feature</span>
            </div>
          </div>

          <Card.Body>
            <Card.Title className="cardTitle">Exploring Lakan Umali's Poetic Landscape</Card.Title>
            <Card.Text className="cardText">
              <ShortenedText text="Diving deep into the captivating and evocative poetry of Lakan Umali, we enter a realm where words effortlessly weave together to form a vibrant tapestry that embraces the LGBT experience. Lakan Umali's poetic world becomes a sanctuary for self-expression, exploring the intricacies of love, identity, and the challenges faced by the LGBTQ+ community." 

                    link="https://www.scoutmag.ph/34974/leaders-of-the-new-cool-lakan/" />
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>


    )
}



