import React, { useState, useEffect } from 'react';

// MATERIAL UI
// import Box from '@mui/material/Box';
// import Drawer from '@mui/material/Drawer';
// import Button from '@mui/material/Button';
// import List from '@mui/material/List';
// import Divider from '@mui/material/Divider';
// import ListItem from '@mui/material/ListItem';
// import ListItemButton from '@mui/material/ListItemButton';
// import ListItemIcon from '@mui/material/ListItemIcon';
// import ListItemText from '@mui/material/ListItemText';
// import InboxIcon from '@mui/icons-material/MoveToInbox';
// import MailIcon from '@mui/icons-material/Mail';

import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';


const AddToCartPage = () => {
  const [products, setProducts] = useState([]);
  const [selectedProduct, setSelectedProduct] = useState('');
  const [quantity, setQuantity] = useState(1);
  const [cart, setCart] = useState(null);
  const [error, setError] = useState('');

  useEffect(() => {
    // Fetch products from the server
    fetchProducts();
    // Fetch user's cart from the server
    fetchCart();
  }, []);
// *****************************************************************

  // const fetchProducts = async () => {
  //   try {
  //     // Replace this with your own fetch logic
  //     const response = await fetch('localhost:4000/cart');
  //     const data = await response.json();
  //     setProducts(data);
  //   } catch (error) {
  //     setError('Failed to fetch products');
  //   }
  // };

// *****************************************************************



   const fetchProducts = async () => {


    fetch(`http://localhost:4000/product/all`)
   	.then(res => res.json())
   	.then(data => {
   		console.log(data);

   		setProducts(data)
   	})
   }
;

// ------------------------------------------------------------


const fetchCart = async () => {

  

        // Process a fetch request to the corresponding API

        fetch(`http://localhost:4000/cart`, {
            method: "GET",
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            // If no user info is found, the "access" property will not be available
            if(typeof data.access !== "undefined"){
                localStorage.setItem("token", data.access);
                retrieveUserDetails(data.access);

                setProducts(data)

            }else{
                setError('Failed to fetch products')
            }
        })
// ------------------------------------------------------------

  //  const fetchProducts = async () => {
  //   try {
  //     // Replace this with your own fetch logic
  //     const response = await fetch('localhost:4000/cart');
  //     const data = await response.json();
  //     setProducts(data);
  //   } catch (error) {
  //     setError('Failed to fetch products');
  //   }
  // };

// ------------------------------------------------------------




  // const fetchProducts = async () => {
  //   try {
  //     // Replace this with your own fetch logic
  //     const response = await fetch('http://localhost:4000/product/all');
  //     const data = await response.json();
  //     setCart(data);
  //   } catch (error) {
  //     setError('Failed to fetch cart');
  //   }
  // };
// *****************************************************************



  const handleProductChange = (event) => {
    setSelectedProduct(event.target.value);
  };

  const handleQuantityChange = (event) => {
    setQuantity(event.target.value);
  };

  const handleAddToCart = async () => {
    try {
      // Replace this with your own fetch logic
      await fetch('/api/cart', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          productId: selectedProduct,
          quantity: quantity,
        }),
      });
      // Refresh the cart after adding the product
      fetchCart();
    } catch (error) {
      setError('Failed to add product to cart');
    }
  };


  // MATERIAL UI************************


// export default function TemporaryDrawer() {
//   const [state, setState] = React.useState({
//     top: false,
//     left: false,
//     bottom: false,
//     right: false,
//   });

//   const toggleDrawer = (anchor, open) => (event) => {
//     if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
//       return;
//     }

//     setState({ ...state, [anchor]: open });
//   };

//   const list = (anchor) => (
//     <Box
//       sx={{ width: anchor === 'top' || anchor === 'bottom' ? 'auto' : 250 }}
//       role="presentation"
//       onClick={toggleDrawer(anchor, false)}
//       onKeyDown={toggleDrawer(anchor, false)}
//     >

//     <h1> Shopping Cart </h1>
//       <List>

//       <Form onSubmit={e => checkOut(e)}>


// 	     <Row>
// 		     <Col xs={6} md={6} lg={6} className="d-flex justify-content-center">
// 			     <div className="input-with-image">
// 			     	<Image className="editModal-img-fit mb-2 image-preview "
// 			              src={image}
// 			          	/>
// 			      </div>

// 	      	</Col>

// 	      	<Col xs={6} md={6} lg={6}>
// 		      	<Card.Header className='py-3 my-3 '>
// 	                <Card.Title>
// 	                    {productName} 
// 	                </Card.Title>

// 	            </Card.Header>
	            
// 	            <Card.Body>
// 		            <Card.Subtitle>
// 		                Price
// 		            </Card.Subtitle>
		            
// 		            <Card.Text>
// 		                Php {price}
// 		            </Card.Text>
		         
// 					<Card.Subtitle>
// 		                Quantity
// 		            </Card.Subtitle>

// 					<Card.Text>
// 		                {quantity}
// 		            </Card.Text>

// 		            <Card.Subtitle>
// 		                Subtotal: {quantity}
// 		            </Card.Subtitle>
					
// 	            </Card.Body>
// 			<div className="text-center">
           
// 		    </Col>



       
//       </List>
//       <Divider />
//       <List>

// 			<Card.Footer>
			
//             {
				
// 				(user.id !== null)
// 				?
// 					<Button id="checkOutBtn" className='w-50 btn-dark my-3 shadow' onClick={() => buy(productId)}>CHECK OUT</Button>
// 				:
// 					<Button className='w-50 btn-danger my-3 shadow' as={Link} to={`/login`}>LOGIN TO BUY</Button>
// 			}
//             </Card.Footer>
        
//       </List>
//     </Box>
//   );

  return (
  	<>

  		<h1> Shopping Cart </h1>


  		<Button variant="contained">Checkout</Button>

  		<div id="header-checkout">
	  		<p>Item</p>
	  		<p>Qty</p>
	  		<p>Price</p>
  		</div>

  		<Divider />





  			 <div className="input-with-image">
		     	<Image className="editModal-img-fit mb-2 image-preview "
		              src={image}
		        />

		        <div id="cart-prod-text" className="d-flex flex-column">
		        	<h4>{name}</h4>
		        	<p>{category}</p>
		        </div>


		        <table class="tg">
		        <thead>
		          <tr>
		            <th class="tg-0pky" colspan="2">Item</th>
		            <th class="tg-0pky">Qty</th>
		            <th class="tg-0pky">Price</th>
		            <th class="tg-0pky"></th>
		          </tr>
		        </thead>
		        <tbody>
		          <tr>
		            <td class="tg-0pky">
		            	<Image className="editModal-img-fit mb-2 image-preview "
		              		src={image}
		        		/>
		        	</td>
		            <td class="tg-0pky">
		            	<div id="cart-prod-text" className="d-flex flex-column">
		            		<h4>{name}</h4>
		            		<p>{category}</p>
		            	</div>

		            </td>
		            <td class="tg-0pky"></td>
		            <td class="tg-0pky"></td>
		            <td class="tg-0pky"></td>
		          </tr>
		          <tr>
		            <td class="tg-0pky" colspan="3"></td>
		            <td class="tg-0pky"></td>
		            <td class="tg-0pky"></td>
		          </tr>
		        </tbody>
		        </table>
		     




		          	
		     </div>

	        <Card className="my-3 w-100  card-height shadow card-border shadow-md card-bg ">
	            <Card.Img className='product-img-fit'
	                src={imgSource}
	            />
	                <Card.Header className='py-3 my-3 '>
	                <Card.Title>
	                    {productBrand} {productName} - {productModel}
	                </Card.Title>
	                </Card.Header>
	                <Card.Body>
	                <Card.Subtitle>
	                    Description
	                </Card.Subtitle>
	                <Card.Text>
	                    {description}
	                </Card.Text>
	                <Card.Subtitle>
	                    Price
	                </Card.Subtitle>
	                <Card.Text>
	                    Php {price}
	                </Card.Text>
	                <Card.Subtitle>
	                    Stocks
	                </Card.Subtitle>
					
	                <Card.Text>
	                    {stocks} available
	                </Card.Text>
					<Card.Subtitle>
	                    Quantity
	                </Card.Subtitle>
					<Form.Control className="w-25 my-2"
	                    type="number"
	                    required
						value={quantity}
	                />
	                </Card.Body>
				<div className="text-center">
	            <Card.Footer>
				
	            {
					
					(user.id !== null)
					?
						<Button id="checkOutBtn" className='w-50 btn-dark my-3 shadow' onClick={() => buy(productId)}>CHECK OUT</Button>
					:
						<Button className='w-50 btn-danger my-3 shadow' as={Link} to={`/login`}>LOGIN TO BUY</Button>
				}
	            </Card.Footer>
				</div>
				

	        </Card>










</>




















// *********************MATERIAL UI***********************

//   	<div>
//   	      {['left', 'right', 'top', 'bottom'].map((anchor) => (
//   	        <React.Fragment key={anchor}>
//   	          <Button onClick={toggleDrawer(anchor, true)}>{anchor}</Button>
//   	          <Drawer
//   	            anchor={anchor}
//   	            open={state[anchor]}
//   	            onClose={toggleDrawer(anchor, false)}
//   	          >
//   	            {list(anchor)}
//   	          </Drawer>
//   	        </React.Fragment>
//   	      ))}
//   	    </div>
//   	  );



//   	<Modal
//                 size="lg"
//                 aria-labelledby="contained-modal-title-vcenter"
//                 centered
//                 show={modalEdit}
//                 >
//                 <Form onSubmit={e => editProduct(e)}>

//                         <Modal.Header className="banner-bg text-light">
//                             <Modal.Title>EDIT PRODUCT</Modal.Title>
//                         </Modal.Header>

//                         <Modal.Body>
//                         <div className="input-with-image d-flex justify-content-center">
//                             <Image className="editModal-img-fit mb-2 image-preview "
//                                 src={image}
//                             />
//                         </div>
//                             <Form.Group controlId="productName" className="mb-3">
//                                 <Form.Label>Product Name</Form.Label>
//                                 <Form.Control
//                                     type="text"
//                                     placeholder="Enter Product Name"
//                                     value={name}
//                                     onChange={e => setName(e.target.value)}
//                                     required
//                                 />
//                             </Form.Group>

//                             <Form.Group controlId="description" className="mb-3">
//                                 <Form.Label>Product Description</Form.Label>
//                                 <Form.Control
//                                     as="textarea"
//                                     rows={3}
//                                     placeholder="Enter Product Description"
//                                     value={description}
//                                     onChange={e => setDescription(e.target.value)}
//                                     required
//                                 />
//                             </Form.Group>

//                             <Form.Group controlId="price" className="mb-3">
//                                 <Form.Label>Product Price</Form.Label>
//                                 <Form.Control
//                                     type="number"
//                                     placeholder="Enter Product Price"
//                                     value={price}
//                                     onChange={e => setPrice(e.target.value)}
//                                     required
//                                 />
//                             </Form.Group>
// {/*
//                             <Form.Group controlId="stocks" className="mb-3">
//                                 <Form.Label>Product Stocks</Form.Label>
//                                 <Form.Control
//                                     type="number"
//                                     placeholder="Enter Product Stocks"
//                                     value={stocks}
//                                     onChange={e => setStocks(e.target.value)}
//                                     required
//                                 />
//                             </Form.Group>*/}
//                             <Form.Group controlId="image" className="mb-3">
//                                 <Form.Label>Image Link</Form.Label>
//                                 <Form.Control
//                                     type="text"
//                                     placeholder="Enter Product Image Link"
//                                     value={image}
//                                     onChange={e => setImage(e.target.value)}
//                                     required
//                                 />
//                             </Form.Group>
//                         </Modal.Body>

//                         <Modal.Footer>
//                             {isActive
//                                 ?
//                                 <Button variant="primary" type="submit" id="submitBtn">
//                                     Save
//                                 </Button>
//                                 :
//                                 <Button variant="danger" type="submit" id="submitBtn" disabled>
//                                     Save
//                                 </Button>
//                             }
//                             <Button variant="secondary" onClick={closeEdit}>
//                                 Close
//                             </Button>
//                         </Modal.Footer>

//                     </Form>
//                 </Modal>




    // <div>
    //   <h1>Add to Cart</h1>
    //   {error && <p>{error}</p>}
      


    //   <div>
    //     <label htmlFor="quantity">Quantity:</label>
    //     <input
    //       type="number"
    //       id="quantity"
    //       value={quantity}
    //       onChange={handleQuantityChange}
    //       min="1"
    //     />
    //   </div>
    //   <button onClick={handleAddToCart} disabled={!selectedProduct}>
    //     Add to Cart
    //   </button>
    //   {cart && (
    //     <div>
    //       <h2>Cart</h2>
    //       <p>Total items: {cart.items.length}</p>
    //       <p>Total price: {cart.totalPrice}</p>
    //     </div>
    //   )}
    // </div>
  );
};

export default AddToCartPage;
