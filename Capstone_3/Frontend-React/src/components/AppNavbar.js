import {Nav, Navbar, Dropdown} from "react-bootstrap";
// import { FormText } from "react-bootstrap";
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { Link, NavLink } from "react-router-dom";
import { useState, useEffect, useContext } from "react";
import {Col} from "react-bootstrap"

import UserContext from "../UserContext";
// import { FaShoppingCart } from 'react-icons/fa';
// import { CartFill } from 'react-bootstrap-icons';

//****************************FONTAWESOME IMPORTS********************

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
// import { faSearch } from '@fortawesome/free-solid-svg-icons';
// import Swal from "sweetalert2";

//****************************CART DRAWER CARDS IMPORTS********************

import CartProdCard from "./CartProdCard";

import "./components.css";


//****************************MATERIAL UI IMPORTS********************

import SearchRoundedIcon from '@mui/icons-material/SearchRounded';


//****************************CART DRAWER IMPORTS********************

import * as React from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
// import ListItemButton from '@mui/material/ListItemButton';
// import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
// import InboxIcon from '@mui/icons-material/MoveToInbox';
// import MailIcon from '@mui/icons-material/Mail';

























export default function AppNavbar(){

  // State to store user info stored in the login page
  /*const [user, setUser] = useState(localStorage.getItem("email"));
  console.log(user);*/

  const { user } = useContext(UserContext);

  


  // const [userInfo, setUserInfo] = useState([])

  const [allOrderProduct, setAllOrderProduct] = useState([])

  const [total, setTotal] = useState(0)

  const [productId, setProdId] = useState("");


 const [newQuantity, setQuantity] = useState("");

  



  
    

    const removeCart = async (prodId) => {
      const token = localStorage.getItem('token');

      await fetch(`http://localhost:4000/cart/${prodId}`, {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${token}`
        }
      })
        .then(res => res.json())
        .then(data => {
          console.log(data);

          setAllOrderProduct(prevProducts => {
                  const updatedProducts = prevProducts.filter(product => product.key !== prodId);
                  
                  const updatedTotal = prevProducts.reduce((total, product) => total + product.subtotal, 0);
                setTotal(updatedTotal);

                  return updatedProducts;
                });

           
             
          

          // Fetch the updated total after removing the item
                fetch(`http://localhost:4000/cart`, {
                  method: 'POST',
                  headers: {
                    Authorization: `Bearer ${token}`
                  }
                })
                  .then(res => res.json())
                  .then(data => {
                    setTotal(data.total);
                  })
                  .catch(error => {
                    console.error('Error getting total:', error);
                  });
              

        })
        .catch(error => {
          console.error('Error removing item from cart:', error);
        });
    };










    // **************GET CURRENT CART**********************

    useEffect(() => {
      const token = localStorage.getItem('token');

      fetch(`http://localhost:4000/cart`, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
        .then(res => res.json())
        .then(data => {
          console.log(`this is the User cart: ${data}`);
          console.log({data});

          if (Array.isArray(data)) {
            
          // Fetch the product details for each order
          const productPromises = data.map(order => 
            fetch(`http://localhost:4000/product/search/${order.productId}`)
              .then(res => res.json())
              .then(prod => ({

                _id: prod._id,
                productId: order.productId,
                image: prod.image,
                name: prod.name,
                price: prod.price,
                category: prod.category,
                quantity: order.quantity,
                subtotal: order.subtotal

              }))
          );
          Promise.all(productPromises)
            .then(products => {
              setProdId(products.prodId)
              const cartProdCards = products.map(product => (
                <Col key={product._id} className="my-5 mx-3">
                  <CartProdCard orderProp={product}  onRemove={removeCart} />
                </Col>
              ));

              setAllOrderProduct(cartProdCards);
            })
            .catch(error => {
              console.error("Error fetching product details:", error);
            });
        }else {
          setTotal(0)
        }
        });
    }, []);

    
    // useEffect(() => {
    //   const token = localStorage.getItem('token');

    //   fetch(`http://localhost:4000/cart`, {
    //     method: "GET",
    //     headers: {
    //       Authorization: `Bearer ${token}`
    //     }
    //   })
    //     .then(res => res.json())
    //     .then(data => {
    //       console.log(`this is the User cart: ${data}`);
    //       console.log(data);

    //       // Fetch the product details for each order
    //       Promise.all(data.map(order => (
    //         fetch(`http://localhost:4000/product/search/${order.productId}`)
    //           .then(res => res.json())
    //           .then(prod => ({


    //             _id: prod._id,
    //             productId: order.productId,
    //             image: prod.image,
    //             name: prod.name,
    //             price: prod.price,
    //             category: prod.category,
    //             quantity: order.quantity,
    //             subtotal: order.subtotal
    //           }))

              

    //       )))
    //         .then(products => {
    //           const cartProdCards = products.map(product => (
    //             <Col key={product._id} className="my-5 mx-3">
    //               <CartProdCard orderProp={product} />
    //             </Col>
    //           ));
    //           setAllOrderProduct(cartProdCards);
    //         })
    //         .catch(error => {
    //           console.error("Error fetching product details:", error);
    //         });
    //     });
    // }, []);


    
    //*******************TOTAL AMOUNT OF CART************* 

    useEffect(() => {
      const token = localStorage.getItem('token');

      fetch(`http://localhost:4000/cart`, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
        .then(res => res.json())
        .then(data => {

          if (data.total !== undefined) {
            setTotal(data.total);
            console.log(data)
            console.log(data.total)
          }else {
            setTotal(0)
          }
              
              })
            
            .catch(error => {
              console.error('Error getting total:', error);
            });

          // ...
        
    }, []);






    const [prevScrollPos, setPrevScrollPos] = useState(0);
      const [visible, setVisible] = useState(true);

      useEffect(() => {
        const handleScroll = () => {
          const currentScrollPos = window.pageYOffset;
          // const scrollDirection = prevScrollPos > currentScrollPos ? 'up' : 'down';
          const isMiddle = currentScrollPos > window.innerHeight / 2;

          if (isMiddle) {
            setVisible(false);
          } else {
            setVisible(true);
          }

          setPrevScrollPos(currentScrollPos);
        };

        window.addEventListener('scroll', handleScroll);

        return () => {
          window.removeEventListener('scroll', handleScroll);
        };
      }, [prevScrollPos]);


      //****************************CART DRAWER CARDS********************

      

      // useEffect(() => {
      //     fetch(`http://localhost:4000/product/all`)
      //     .then(res => res.json())
      //     .then(data => {
      //       console.log(data);

      //       setAllProduct(data.map(product => {
      //         return (
      //           <Col className="d-inline-block mx-2">
      //           <CartProdCard className="mx-3" key={product._id} productProp={product}/>
      //           </Col>
      //           )
      //       }))


      //     })
      //   }, [])
 


//****************************CART DRAWER <start>********************
// Define the state outside of the component rendering the drawer
const initialState = {
  right: false
};

  const [state, setState] = React.useState(initialState);

    const toggleDrawer = (anchor, open) => (event) => {
       if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
         return;
       }

      setState({ ...state, [anchor]: open });
    };

    const list = (anchor) => (
        <Box
            sx={{ width: 300 }}
            role="presentation"
           
            
          >
            <Box sx={{ position: 'sticky', top: 0, backgroundColor: '#fff' }}>
              {/* Fixed header */}
              <List>
                <ListItem disablePadding>
                  <ListItemText 
                    primary={<span style={{ fontFamily: 'Arial', fontSize: '1.6rem', fontWeight: 'bolder' }}>Your cart</span>} 
                    id="cart-heading"
                    className="mx-4"/>
                </ListItem>
              </List>
            </Box>

            <div className="d-flex justify-content-center align-items-center">

            <Divider className="w-75 bg-dark "/>
            </div>

            <Box sx={{ overflowY: 'scroll', maxHeight: 'calc(100vh - 200px)' }}>
              {/* Scrollable content */}
              <List>
                {allOrderProduct}
              </List>
            </Box>

            <Box sx={{ position: 'sticky', bottom: 0, backgroundColor: '#fff' }}>
              {/* Fixed "total" text */}
              <List>
                <ListItem disablePadding>
                  <ListItemText primary={`Total: ₱${total}`} />

                  {
        
                    (user._id !== null)
                    ?
                      <Button id="checkOutBtn" className='w-50 btn-dark my-3 mx-3 shadow' as={Link} to={`/checkout`} >CHECK OUT</Button>
                    :
                      <Button className='w-50 btn-danger my-3 shadow' as={Link} to={`/login`}>LOGIN TO BUY</Button>
                  }

                </ListItem>
              </List>
            </Box>

            <Divider />
          </Box>
    );



//****************************CART DRAWER <end>********************














  return(
  <Navbar  

    expand="lg" 
    className=" py-3" 
    className={`navbar ${visible ? '' : 'hidden'}`} 
    id="appnavbar-all">


      <Navbar.Brand as={Link} to={"/"} className="fw-bold" id="logo-name">zine.repo</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ms-auto " id="navText">
          <Nav.Link className="mx-4 navlink" as={NavLink} to={"/"}>Home</Nav.Link>
          <Nav.Link className="mx-4 navlink" as={NavLink} to={"/products"}>Zines & Books</Nav.Link>
          <Nav.Link className="mx-4 navlink" as={NavLink} to={"#"}>Recommendations</Nav.Link>
          <Nav.Link className="mx-4 navlink" as={NavLink} to={"#"}>About Us</Nav.Link>

           <Form className="d-flex" id="appnavbar-searchbox">
            <Form.Control
              type="search"
              placeholder="search"
              className="me-2"
              aria-label="Search"
              id="search-box"
            />
            <Button variant="outline-none" id="search-text">
              <SearchRoundedIcon id="search-icon"/>
            </Button>
          </Form>

          

          <Dropdown id="account-dropdown-head">
                <Dropdown.Toggle variant="none" id="account-dropdown">
                  <i className="fa-solid fa-user" id="account-icon"></i>
                </Dropdown.Toggle>

                <Dropdown.Menu id="account-dropdown-menu">
                {
                  (user.isAdmin === true) ?
                    <Dropdown.Item className="account-option" as={NavLink} to={"/allProducts"}>Account</Dropdown.Item>
                    :
                    <Dropdown.Item className="account-option" as={NavLink} to={"/products"}>Account</Dropdown.Item>


                }
                  
                  <Dropdown.Item className="account-option" href="#settings">Settings</Dropdown.Item>


                  

                  
                    {
                      (user.token !== null) ?
                      <Dropdown.Item className="account-option" as={NavLink} to={"/logout"}>Logout</Dropdown.Item>
                    :
                    <>
                      
                      <Dropdown.Item className="account-option" as={NavLink} to={"/register"}>Register</Dropdown.Item>
                      <Dropdown.Item className="account-option" as={NavLink} to={"/login"}>Login</Dropdown.Item>
                    </>
                    }


                  
                </Dropdown.Menu>
              </Dropdown>

             {/* {

                (user.token !== null) ?

                <Nav.Link className="mx-1 mb-0" as={NavLink} to={"/products"}> 
                
                    <FontAwesomeIcon icon={faShoppingCart} id="cart-icon"/>
                
                </Nav.Link>

                :
                <Nav.Link className="mx-1 mb-0" as={NavLink} to={"/products"}> 
                    
                    <FontAwesomeIcon icon={faShoppingCart} id="cart-icon"/>
                
                </Nav.Link>

              }*/}


              <div >
                    

                        <Button 
                       id="cart-btn" 
                       onClick={toggleDrawer('right', true)}>
                          
                          {

                            (user.token !== null) ?

                            <Nav.Link className="mx-1 mb-0" as={NavLink} to={"#"}> 
                            
                                <FontAwesomeIcon icon={faShoppingCart} id="cart-icon"/>
                            
                            </Nav.Link>

                            :
                            <Nav.Link className="mx-1 mb-0" as={NavLink} to={"#"}> 
                                
                                <FontAwesomeIcon icon={faShoppingCart} id="cart-icon"/>
                            
                            </Nav.Link>

                          }



                        </Button>
                        
                        <Drawer
                          anchor="right"
                          open={state['right']}
                          onClose={toggleDrawer('right', false)}
                          // onOpen={toggleDrawer('right', true)}
                        >
                          {list('right')}
                        </Drawer>

                      
                    
                  </div>
























              
              {/*<Nav.Link className="mx-1 mb-0" as={NavLink} to={"/cart"}><CartFill id="account-icon"/></Nav.Link>*/}

          {/*{
            (user.token !== null) ?
            <Nav.Link as={NavLink} to={"/logout"}>Logout</Nav.Link>
          :
          <>
            <Nav.Link as={NavLink} to={"/register"}>Register</Nav.Link>
            <Nav.Link as={NavLink} to={"/login"}>Login</Nav.Link>
          </>
          }*/}

         

        </Nav>
      </Navbar.Collapse>
  </Navbar>
    )
}