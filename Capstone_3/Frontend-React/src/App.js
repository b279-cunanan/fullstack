import './App.css';
import AppNavbar from "./components/AppNavbar";
/*import Banner from "./components/Banner"
import Highlights from "./components/Highlights"*/
import Home from "./pages/Home"
import Products from "./pages/Products"
import Register from "./pages/Register"
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
// import Dashboard from './pages/Dashboard';
import AllProducts from './pages/AllProducts';
// import CheckOut from './pages/CheckOut';
import ProductView from './components/ProductView';
import OrderView from './components/OrderView';
// import AppSideNav from './components/AppSideNav';
// import HomeBanner from './components/HomeBanner';








// npm install react-router-dom
import { Route, Routes } from "react-router-dom"
import { BrowserRouter as Router } from "react-router-dom"
import { Container } from "react-bootstrap";
import { useState, useEffect } from "react";
import { UserProvider } from "./UserContext"

// React JS is a single page application (SPA)
// Whenever a link is clicked, it functions as if the page is being reloaded but what it actually does is it goes through the process of rendering, mounting, rerendering and unmounting components
// When a link is clicked, React JS changes the url of the application to mirror how HTML accesses its urls
// It renders the component executing the function component and it's expressions
// After rendering it mounts the component displaying the elements
// Whenever a state is updated or changes are made with React JS, it rerenders the component
// Lastly, when a different page is loaded, it unmounts the component and repeats this process
// The updating of the user interface closely mirrors that of how HTML deals with page navigation with the exception that React JS does not reload the whole page

function App() {

  // // Creating a user state for global scope
  // const[user, setUser] = useState({
  //   email: localStorage.getItem("email")
  // })

  // Creating a user state for global scope
  const[user, setUser] = useState({
    id: null,
    isAdmin: null,
    email: null,
    token: localStorage.getItem("token")
  })

  // Funstion for clearing the storage on logout
  const unSetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (
    <UserProvider value={{user, setUser, unSetUser}}>
      <Router>
          
              <AppNavbar/>
          
          <Container className="">
              <Routes>
                  <Route path="/" element={<Home/>}/>
                  <Route path="/products" element={<Products/>}/>
                  <Route path="/checkout" element={<OrderView/>}/>
                  {/*<Route path="/checkout2" element={<CheckOut/>}/>*/}
                  <Route path="/productView/:productId" element={<ProductView/>}/>
                  <Route path="/register" element={<Register/>}/>
                  <Route path="/login" element={<Login/>}/>
                  <Route path="/logout" element={<Logout/>}/>
                  {/*<Route path="/dashboard" element={<Dashboard/>}/>*/}
                  <Route path="/allProducts" element={<AllProducts/>}/>
                  <Route path="*" element={<Error/>}/>
              </Routes>
          </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
