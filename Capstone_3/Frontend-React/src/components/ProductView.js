import {useState, useEffect, useContext} from "react"
import {Container, Card, Button, Row, Col} from "react-bootstrap"
import { useParams } from "react-router-dom"
import { Link } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2"

export default function ProductView(){

	const { user } = useContext(UserContext);

	// module that allows us to retrieve the courseId passed via URL
	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [image, setImage] = useState(0);
	const [category, setCategory] = useState(0);
	const [rating, setRating] = useState(0);

	useEffect(() => {
		console.log(productId);

		fetch(`http://localhost:4000/product/search/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setImage(data.image);
			setCategory(data.category);
			setRating(data.rating);
			
		})

	}, [productId])

const [cartItems, setCartItems] = useState([]);

	const addToCart = async (productId) => {

	await fetch(`http://localhost:4000/cart/${productId}`, {
			method: "POST",
			headers: {
				"Content-type" : "application/json",
				Authorization : `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

		    setCartItems((prevCart) => [prevCart, data]);
		           console.log("Updated cart items:", cartItems); // Log the updated cart items
		         })
		         .catch((error) => {
		           console.error("Error adding item to cart:", error);
		         });
		};

			//***********CART CONTENTS (start)**************** 

			useEffect(() => {
			  const token = localStorage.getItem("token");

			  fetch(`http://localhost:4000/cart`, {
			    method: "GET",
			    headers: {
			      Authorization: `Bearer ${token}`,
			    },
			  })
			    .then((res) => res.json())
			    .then((data) => {
			      console.log(`this is the User cart: ${data}`);
			      console.log(data);

			      // Fetch the product details for each order
			      Promise.all(
			        data.map((order) =>
			          fetch(`http://localhost:4000/product/search/${order.productId}`)
			            .then((res) => res.json())
			            .then((prod) => ({
			              _id: prod._id,
			              productId: order.productId,
			              image: prod.image,
			              name: prod.name,
			              price: prod.price,
			              category: prod.category,
			              quantity: order.quantity,
			              subtotal: order.subtotal,
			            }))
			        )
			      )
			        .then((products) => {
			        	console.log(products)
			          setCartItems(products);

			        })
			        .catch((error) => {
			          console.error("Error fetching product details:", error);
			        });
			    })
			    .catch((error) => {
			      console.error("Error fetching cart items:", error);
			    });
			}, []);
			

			//***********CART CONTENTS (end)****************





// ******************RATING FUNCTION***************** 

	const roundedRating = Math.round(rating * 2) / 2;

	  // Generate an array of star icons based on the rating
	  const starIcons = [];
	  for (let i = 0; i < 5; i++) {
	    if (roundedRating >= i + 1) {
	      starIcons.push(<i key={i} className="fas fa-star"></i>);
	    } else if (roundedRating >= i + 0.5) {
	      starIcons.push(<i key={i} className="fas fa-star-half-alt"></i>);
	    } else {
	      starIcons.push(<i key={i} className="far fa-star"></i>);
	    }
	  } 






	return(
		<Container className="mt-5 p-5 m-5" id="productview-containter">
			<Row>
			<Card.Img variant="top" src={image} id="image-product-view" />
				<Col className="d-flex justify-content-center align-items-center">
					<Card id="productview-text">
						<Card.Body className="text-center">
							<Card.Title className="my-5" id="prodView-name">{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Category:</Card.Subtitle>
							<Card.Text>{category}</Card.Text>
							<Card.Subtitle>Rating:</Card.Subtitle>
							<Card.Text className="rating">{starIcons}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							{/*<Button variant="primary" onClick={() => addToCart(productId)}>Add to Cart</Button>*/}
							{

								(user.isAdmin === true) ?

								<Link className="btn btn-danger" to={`/products`}>Go back</Link>

								:

								<Link className="btn btn-primary" to={`/products`} onClick={() => addToCart(productId)}>Add to cart</Link>	
							}
							
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
		);
}







