const mongoose = require('mongoose');

const sellerSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  contactInfo: {
    email: {
      type: String,
      required: true
    },
    phone: {
      type: String,
      required: true
    }
  },
  additionalDetails: {
    type: String
  }
});

 

module.exports = mongoose.model('Seller', sellerSchema);
