const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");


// Route for user check email
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details

router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	userController.detailsUser(userData.id).then(resultFromController => res.send(resultFromController));

});


// RETRIEVE USER ORDER DETAILS
router.post("/orders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	// Provides the user's ID for the getProfile controller method
	userController.orderDetailsUser(userData.id).then(resultFromController => res.send(resultFromController));

});

// USERS LIST
router.get("/", (req, res) => {
	userController.getAllUser().then(resultFromController => res.send(resultFromController));
});


// // CHECKOUT product of non-admin user 
// router.post("/checkout", auth.verify,(req, res) => {
// 	let data = {
		
// 		products : req.body
// 	}

// 	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
// 	let userIdAuth = auth.decode(req.headers.authorization).id;
	
// 	userController.checkout(data, userIdAuth, isAdmin).then(resultFromController => res.send(resultFromController));
// })





// SET USER AS ADMIN
router.put("/updatestatus", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.updateUser(req.body, isAdmin).then(resultFromController => res.send(resultFromController));
})


// Delete a user
router.put("/:userId/delete", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.deleteUser(req.params.id, userData).then(resultFromController => res.send(resultFromController));
})

// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;