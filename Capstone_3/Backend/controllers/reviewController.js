const Review = require('../models/review');
const Product = require('../models/product'); 

// #############################################################################

// Create a review
exports.createReview = async (req, res) => {
  const { productId, userId, rating, comment } = req.body;

  try {
    // Check if the product exists
    const product = await Product.findById(productId);
    if (!product) {
      return res.status(404).json({ error: 'Product not found.' });
    }

    // Create a new review
    const review = new Review({
      productId,
      userId,
      rating,
      comment
    });

    // Save the review
    await review.save();

    res.json({ message: 'Review created successfully.' });
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while creating the review.' });
  }
};

// #############################################################################

// Retrieve all reviews
exports.getAllReviews = (req, res) => {
  Review.find()
    .then(reviews => {
      res.json(reviews);
    })
    .catch(error => {
      res.status(500).json({ error: 'Failed to retrieve reviews.' });
    });
};

// #############################################################################


// Retrieve a specific review
exports.getReviewById = (req, res) => {
  const { reviewId } = req.params;

  Review.findById(reviewId)
    .then(review => {
      if (!review) {
        return res.status(404).json({ error: 'Review not found.' });
      }
      res.json(review);
    })
    .catch(error => {
      res.status(500).json({ error: 'Failed to retrieve the review.' });
    });
};

// #############################################################################

// Update a specific review
exports.updateReview = (req, res) => {
  const { reviewId } = req.params;
  const { title, content } = req.body;

  Review.findByIdAndUpdate(
    reviewId,
    { title, content },
    { new: true }
  )
    .then(updatedReview => {
      if (!updatedReview) {
        return res.status(404).json({ error: 'Review not found.' });
      }
      res.json(updatedReview);
    })
    .catch(error => {
      res.status(500).json({ error: 'Failed to update the review.' });
    });
};

// #############################################################################

// Get reviews for a product
exports.getProductReviews = async (req, res) => {
  const { productId } = req.params;

  try {
    const reviews = await Review.find({ productId }).populate('userId', 'name');
    if (!reviews) {
      return res.status(404).json({ error: 'No reviews found for this product.' });
    }

    res.json(reviews);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while fetching the product reviews.' });
  }
};


// #############################################################################
// Delete a review
exports.deleteReview = async (revId) => {
  const { reviewId } = revId;

  try {
    const review = await Review.findByIdAndDelete(reviewId);
    if (!review) {
      return res.status(404).json({ error: 'Review not found.' });
    }

    res.json({ message: 'Review deleted successfully.' });
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while deleting the review.' });
  }
};
