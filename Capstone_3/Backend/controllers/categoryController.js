const Category = require('../models/category');
const Product = require('../models/product');


// ############################################################################# 

// Create a new category
exports.createCategory = async (req, res) => {
  const { name } = req.body;

  try {
    // Check if the category already exists
    const existingCategory = await Category.findOne({ name });
    if (existingCategory) {
      return res.status(400).json({ error: 'Category already exists.' });
    }

    // Create a new category
    const category = new Category({ name });

    // Save the category
    await category.save();

    res.json({ message: 'Category created successfully.' });
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while creating the category.' });
  }
};

// #############################################################################


// Get all categories
exports.getAllCategories = async (req, res) => {
  try {
    const categories = await Category.find();
    res.json(categories);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while fetching the categories.' });
  }
};

// #############################################################################


// Get products by category
exports.getProductsByCategory = async (req, res) => {
  const { categoryId } = req.params;

  try {
    const products = await Product.find({ categoryId });
    if (!products) {
      return res.status(404).json({ error: 'No products found for this category.' });
    }

    res.json(products);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while fetching the products for the category.' });
  }
};

// #############################################################################


// Delete a category
exports.deleteCategory = async (req, res) => {
  const { categoryId } = req.params;

  try {
    // Check if the category has associated products
    const products = await Product.find({ categoryId });
    if (products && products.length > 0) {
      return res.status(400).json({ error: 'Cannot delete a category with associated products.' });
    }

    // Delete the category
    const deletedCategory = await Category.findByIdAndDelete(categoryId);
    if (!deletedCategory) {
      return res.status(404).json({ error: 'Category not found.' });
    }

    res.json({ message: 'Category deleted successfully.' });
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while deleting the category.' });
  }
};
