import { useContext, useState, useEffect } from "react"
import UserContext from "../UserContext";
import {Navigate} from "react-router-dom";
import {Container, Col, Row, Button, Table, Modal, Form, Image} from "react-bootstrap"
import Swal from "sweetalert2";
import AppSideNav from "../components/AppSideNav";
import React from "react";
// import logo from "../images/logo.jpeg"
import HomeBanner from "../components/HomeBanner";
import "../components/components.css";


export default function AllProducts(){

     const data = {
		title: "VIEW ALL PRODUCTS",
		content: "You can Add, edit, and archive a product on this page.",
		destination: "/product/all",
		label: "PRODUCTS"
		// image: {logo}
	}

    const { user } = useContext(UserContext);

    const [allProducts, setAllProducts]= useState([]);

    const [productId, setProductId] = useState("");
    const [name, setName] = useState("");
    // const [productBrand, setProductBrand] = useState("");
    // const [productType, setProductType] = useState("");
    const [category, setCategory] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    // const [stocks, setStocks] = useState(0);
    const [image, setImage] = useState("");

    const [isActive, setIsActive] = useState(false);

    const [modalAdd, setModalAdd] = useState(false);
    const [modalEdit, setModalEdit] = useState(false);


    const addProductOpen = () => setModalAdd(true);
    const addProductClose = () => setModalAdd(false);

    const openEdit = (id) => {
        setProductId(id);

        fetch(`http://localhost:4000/product/search/${id}`)
            .then(res => res.json())
            .then(data => {

                console.log(data);

                setName(data.name);
                setDescription(data.description);
                setPrice(data.price);
                setCategory(data.category);
                // setStocks(data.stocks);
                setImage(data.image);
            });

     setModalEdit(true)
    };

    const closeEdit = () => {

        setName('');
        setDescription('');
        setCategory('');
        setImage('');
        setPrice(0);
        // setStocks(0);

     setModalEdit(false);
    };




    /* GET ALL PRODUCTS */
	const fetchData = () =>{
		fetch(`http://localhost:4000/product/all`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

            setAllProducts(data.map(products =>{
                return (
                    <>
                    
                    <tr key={products._id}>
                        <td>{products._id}</td>
                        <td>{products.name}</td>
                        <td>{products.category}</td>
                        <td>{products.description}</td>
                        <td>{products.price}</td>
                        {/*<td>{products.stocks}</td>*/}
                        <td>{products.isActive ? "Active" : "Inactive"}</td>
                        <td className="">
                            <div className="d-flex  flex-column justify-content-space-around align-items-center h-100 m-auto p-auto">
                            {(products.isActive)
                                ?
                                <>
                                <Button className="my-2" variant="danger" size="sm" onClick={() => archive(products._id, products.name)}>Archive</Button>
                                <Button variant="secondary" className="mx-1 px-3" size="sm" onClick={() => openEdit(products._id)}>Edit</Button>
                                </>
                                :
                                <>
                                <Button variant="success" className="mx-1 my-2" size="sm" onClick={() => unarchive(products._id, products.name)}>Unarchive</Button>
                                <Button variant="secondary" className="mx-1 px-3" size="sm" onClick={() => openEdit(products._id)}>Edit</Button>
                                
                                </>
                            }
                            </div>
                        </td>
                    </tr>
                    </>
                )
            }));
		});
	}


    /* ARCHIVE PRODUCT */
    const archive = (id, name) =>{
        console.log(id);
        console.log(name);
        fetch(`http://localhost:4000/product/archive/${id}`,
        {
            method : "PUT",
            headers : {
                "Content-Type" : "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
        body: JSON.stringify({
            isActive: false
        })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data){
                Swal.fire({
                    title: "PRODUCT ARCHIVING SUCCESS!",
                    icon: "success",
                    text: `The product is now in archive`
                });

                fetchData();
            }else{
                Swal.fire({
                    title: "Archive Unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later"
                })
            }
        })
    }

    const unarchive = (id, name) =>{
        console.log(id);
        console.log(name);
        fetch(`http://localhost:4000/product/activate/${id}`,
        {
            method : "PUT",
            headers : {
                "Content-Type" : "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
        body: JSON.stringify({
            isActive: true
        })
    
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data){
                Swal.fire({
                    title: "Unarchive Successful",
                    icon: "success",
                    text: `The product is now active`
                });

                fetchData();
            }else{
                Swal.fire({
                    title: "Unarchive Unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later"
                })
            }
        })
    }

	useEffect(()=>{
		fetchData();


	}, [])

    /* ADD PRODUCT */
    const addProduct = (e) => {
        e.preventDefault();

        fetch(`http://localhost:4000/product/create`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                
                // productModel: productModel,
                
                name: name,
                description: description,
                price: price,
                category: category,
                // stocks: stocks,
                image: image
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if (data) {
                    Swal.fire({
                        title: "PRODUCT ADDED SUCCESSFULLY!",
                        icon: "success",
                        text: `"The new product was added to the product list.`,
                    });


                    fetchData();
                    addProductClose();
                }
                else {
                    Swal.fire({
                        title: "ADD PRODUCT UNSSUCCESSFUL!",
                        icon: "error",
                        text: `The system is experiencing trouble at the moment. Please try again later.`,
                    });
                    addProductClose();
                }

            })
        
        // setProductType('');
        setCategory('');
        setName('');
        setImage('');
        setDescription('');
        setPrice(0);
        // setStocks(0);
    }

    /* EDIT PRODUCT */
    const editProduct = (e) => {
        e.preventDefault();

        fetch(`http://localhost:4000/product/update/${productId}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
                category: category,
                image: image
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if (data) {
                    Swal.fire({
                        title: "PRODUCT EDIT SUCCESSFUL!",
                        icon: "success",
                        text: `Product was edited successfully.`,
                    });

                    fetchData();
                    closeEdit();

                }
                else {
                    Swal.fire({
                        title: "PRODUCT EDIT UNSUCCESSFUL!",
                        icon: "error",
                        text: `The system is experiencing trouble at the moment. Please try again later.`,
                    });

                    closeEdit();
                }

            })

        setName('');
        setDescription('');
        setCategory('');
        setPrice(0);
        // setStocks(0);
    }

    useEffect(() => {

        if (name != "" && description != "" && price > 0) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, description, price, category]);




	return(
        <>
         
        <Container fluid className="w-100 d-flex h-100 m-0 p-0">
        <Row className="w-100 m-0 p-0">
        <Col className="bg-white d-flex  pt-3 m-0 shadow" xs={12} md={2} lg={3}>
        <AppSideNav/>
        </Col>
       
        <Col className="colr-bg m-0 p-4 d-flex justify-content-center" xs={12} md={10} lg={9}>
        {
            (user.isAdmin)
		?
        <>
		<div className="w-100 bg-white  rounded shadow-sm shadow-lg p-lg-5 p-md-1">
			<div className="my-2 text-center">
				<h1>VIEW ALL PRODUCTS</h1>
			</div>
            <div className="my-2 text-right">
                    <Button  variant="danger" className="px-5" onClick={addProductOpen}>ADD A PRODUCT</Button>
                </div>
            <Table striped bordered hover className="shadow-lg shadow-sm ">
            <thead className="banner-bg text-light" id="allProd-theadMain">
                <tr id="allProd-theadText">
                <th>ID</th>
                <th>Name</th>
                <th>Category</th>
                <th className="w-25">Description</th>
                <th>Price</th>
                <th>Status</th>
                <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                {allProducts}
            </tbody>
            </Table>
             

                 {/* ADD PRODUCT MODAL */}

                <Modal
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                show={modalAdd}
                >
                <Form onSubmit={e => addProduct(e)}>

                        <Modal.Header className="banner-bg text-light">
                            <Modal.Title>ADD PRODUCT</Modal.Title>
                        </Modal.Header>

                        <Modal.Body>
                        <Row>
                        <Col xs={6} md={6} lg={6} className="d-flex justify-content-center">
                        <div className="input-with-image">
                        <Image className="editModal-img-fit mb-2 image-preview "
                                src={image}
                            />
                        </div>
                            
                           

                            </Col>
                            <Col xs={6} md={6} lg={6}>
                            <Form.Group controlId="ProductName" className="mb-3">
                                <Form.Label>Product Name</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Product Name"
                                    value={name}
                                    onChange={e => setName(e.target.value)}
                                    required
                                />
                            </Form.Group> 
                            <Form.Group controlId="productModel" className="mb-3">
                                <Form.Label>Product Category</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Product Category"
                                    value={category}
                                    onChange={e => setCategory(e.target.value)}
                                    required
                                />
                            </Form.Group>
                            <Form.Group controlId="description" className="mb-3">
                                <Form.Label>Product Description</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    rows={3}
                                    placeholder="Product Description"
                                    value={description}
                                    onChange={e => setDescription(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="price" className="mb-3">
                                <Form.Label>Product Price</Form.Label>
                                
                                <Form.Control
                                    type="number"
                                    placeholder="Product Price"
                                    value={price}
                                    onChange={e => setPrice(e.target.value)}
                                    
                                    required
                                />
                                
                            </Form.Group>

                            {/*<Form.Group controlId="stocks" className="mb-3">
                                <Form.Label>Product Quantity</Form.Label>
                                <Form.Control
                                    type="number"
                                    placeholder="Product Quantity"
                                    value={stocks}
                                    onChange={e => setStocks(e.target.value)}
                                    required
                                />
                            </Form.Group>*/}
                            <Form.Group controlId="image" className="mb-3">
                                <Form.Label>Image Link</Form.Label>
                                
                                <Form.Control
                                    type="text"
                                    placeholder="Product Image Link"
                                    value={image}
                                    onChange={e => setImage(e.target.value)}

                                    required
                                />
                                
                           
                            </Form.Group>
                            </Col>
                            </Row>
                        </Modal.Body>

                        <Modal.Footer>
                            {isActive
                                ?
                                <Button variant="primary" type="submit" id="submitBtn">
                                    ADD PRODUCT
                                </Button>
                                :
                                <Button variant="danger" type="submit" id="submitBtn" disabled>
                                    ADD PRODUCT
                                </Button>
                            }
                            <Button variant="secondary" onClick={addProductClose}>
                                Close
                            </Button>
                        </Modal.Footer>

                    </Form>
                </Modal>
                
                {/* EDIT MODAL */}

                <Modal
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                show={modalEdit}
                >
                <Form onSubmit={e => editProduct(e)}>

                        <Modal.Header className="banner-bg text-light">
                            <Modal.Title>EDIT PRODUCT</Modal.Title>
                        </Modal.Header>

                        <Modal.Body>
                        <div className="input-with-image d-flex justify-content-center">
                            <Image className="editModal-img-fit mb-2 image-preview "
                                src={image}
                            />
                        </div>
                            <Form.Group controlId="productName" className="mb-3">
                                <Form.Label>Product Name</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Enter Product Name"
                                    value={name}
                                    onChange={e => setName(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="description" className="mb-3">
                                <Form.Label>Product Description</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    rows={3}
                                    placeholder="Enter Product Description"
                                    value={description}
                                    onChange={e => setDescription(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="price" className="mb-3">
                                <Form.Label>Product Price</Form.Label>
                                <Form.Control
                                    type="number"
                                    placeholder="Enter Product Price"
                                    value={price}
                                    onChange={e => setPrice(e.target.value)}
                                    required
                                />
                            </Form.Group>
{/*
                            <Form.Group controlId="stocks" className="mb-3">
                                <Form.Label>Product Stocks</Form.Label>
                                <Form.Control
                                    type="number"
                                    placeholder="Enter Product Stocks"
                                    value={stocks}
                                    onChange={e => setStocks(e.target.value)}
                                    required
                                />
                            </Form.Group>*/}
                            <Form.Group controlId="image" className="mb-3">
                                <Form.Label>Image Link</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Enter Product Image Link"
                                    value={image}
                                    onChange={e => setImage(e.target.value)}
                                    required
                                />
                            </Form.Group>
                        </Modal.Body>

                        <Modal.Footer>
                            {isActive
                                ?
                                <Button variant="primary" type="submit" id="submitBtn">
                                    Save
                                </Button>
                                :
                                <Button variant="danger" type="submit" id="submitBtn" disabled>
                                    Save
                                </Button>
                            }
                            <Button variant="secondary" onClick={closeEdit}>
                                Close
                            </Button>
                        </Modal.Footer>

                    </Form>
                </Modal>



		</div>
        </>
		:
		<Navigate to="/" />}

        </Col>
        
        </Row>
    </Container>

    


    </>


	)
}