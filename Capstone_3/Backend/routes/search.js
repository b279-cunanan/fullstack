const express = require('express');
const router = express.Router();

// Import the Search controller
const searchController = require('../controllers/searchController');

// GET /search/:keyword - Search products by keyword
router.get('/:keyword', (req, res) => {
  searchController.searchByKeyword(req, res)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.status(500).send(error.message));
});

// GET /category/:categoryId - Search products by category
router.get('/category/:categoryId', (req, res) => {
  searchController.searchByCategory(req, res)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.status(500).send(error.message));
});

// GET /price/:minPrice/:maxPrice - Search products by price range
router.get('/price/:minPrice/:maxPrice', (req, res) => {
  searchController.searchByPriceRange(req, res)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.status(500).send(error.message));
});

module.exports = router;
