// import productData from "../data/products.js"
import ProductCard from "../components/ProductCard";
import { useEffect, useState } from "react" 
import {Col} from "react-bootstrap"
import "../components/components.css";



export default function Product(){
	// checks to see if mock data is captured
	// console.log (productData);
	// console.log (productData[0]);

	// State that will be used to store courses retreived from db

	const [allProduct, setAllProduct] = useState([])

	// Retrieves the courses from db upon initial render of the "Courses" Component

	useEffect(() => {
		fetch(`http://localhost:4000/product/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProduct(data.map(product => {

				console.log(typeof product);
				return (
					<Col className="d-inline-block mx-2">
					<ProductCard className="mx-3" key={product._id} productProp={product}/>
					</Col>
					)
			}))


		})
	}, [])

	
	return(
		<>
			<div className="banner-products-head p-0 m-0">
			  <img src="https://www.adobomagazine.com/wp-content/uploads/2019/10/KOMURA-HERO-1024x538.jpg" className="p-0 m-0 products-banner-img" alt="Banner" id="products-banner-img"/>
			  <div className="banner-overlay p-0 m-0"></div>
			  <div className="banner-text p-0 m-0">
			    <h1 className="my-5">Zines & Books</h1>

			    <p>Indulge in our edgy yet refined cultural curation, featuring a captivating array of signed, genre-defying, and experimental works that resonate with the vibrant spirit of youth.</p>
			  </div>
			</div>

			<h1 className="my-5 text-center" id="products-newRelease">New Release</h1>
			
			{/*Prop making and prop passing*/}
			<Col className=" text-dark" id="products-all">
			{allProduct}
			</Col>
		</>
		)

}