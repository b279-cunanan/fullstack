const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type: String,
		required : [true, "FIRST NAME is required!"]
	},
	lastName : {
		type: String,
		required : [true, "LAST NAME is required!"]
	},
	email : {
		type: String,
		required : [true, "EMAIL is required!"]
	},
	password : {
		type: String,
		required : [true, "PASSWORD is required!"]
	},
	isAdmin : {
		type: Boolean,
		default : false
	},
	orders: [{

		orderId: {
		    type: String,
		    required: [true, "ORDER ID is required!"] 
		},

		products: [
						{
						      productId: {
						        type: String,
						        required : [true, "PRODUCT ID is required!"] 
						      },
						      quantity: {
						        type: Number,
						        required: [true, "QUANTITY is required!"] 
						      },
						      subtotal: {
						        type: Number,
						        default: 0
						      }
						}
		],
		total: {
		    type: Number,
		    required: [true, "TOTAL is required!"] 
		},
		shippingAddress: {
		    type: String,
		    required: [true, "ADDRESS is required!"] 
		},
		contactNumber: {
		    type: String,
		    required: [true, "CONTACT NUMBER is required!"] 
		},
		orderDate: {
		    type: Date,
		    default : new Date()
		}
}]


}); 



module.exports = mongoose.model("User", userSchema);