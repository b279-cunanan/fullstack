import { useState, useEffect, useContext } from 'react';
import { Row, Col, Form, Button } from 'react-bootstrap';
import { useNavigate,Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {

    const {user} = useContext(UserContext);

    //an object with methods to redirect the user
    const navigate = useNavigate();

    // State hooks to store the values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [isActive, setIsActive] = useState('');

    // Check if values are successfully binded
    console.log(email);
    console.log(password1);
    console.log(password2);

    // Function to simulate user registration
    function registerUser(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        fetch(`http://localhost:4000/user/checkEmail`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            if(data === true){

                Swal.fire({
                    title: 'Duplicate email found',
                    icon: 'error',
                    text: 'Please provide a different email.'   
                });

            } else {

                fetch(`http://localhost:4000/user/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {

                    console.log(data);

                    if(data === true){

                        // Clear input fields
                        setFirstName('');
                        setLastName('');
                        setEmail('');
                        setMobileNo('');
                        setPassword1('');
                        setPassword2('');

                        Swal.fire({
                            title: 'Registration successful',
                            icon: 'success',
                            text: 'Welcome to zine.repo!'
                        });

                        // Allows us to redirect the user to the login page after registering for an account
                        navigate("/login");

                    } else {

                        Swal.fire({
                            title: 'Something wrong',
                            icon: 'error',
                            text: 'Please try again.'   
                        });

                    };

                })
            };

        })

    }

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [firstName, lastName, email, mobileNo, password1, password2]);

    return (
        (user.token !== null) ?
            <Navigate to="/" />
        :

        <>
        <div id="register-container m-0 p-0">
        <div className="d-flex flex-row-reverse justify-content-end w-100">

        <div className="background-image"></div>
        </div>
        <Row className="m-0 p-0 h-100" lg={12}>
            <Col xs={12} md={12} lg={12} className="translucent-form py-5 d-flex justify-content-center align-items-center">

                          

                <div className="mx-3 mx-5 w-25">

                <div className="mb-3" id="register-title">Sign up</div>
                
                <Form className="w-100 form-container " onSubmit={(e) => registerUser(e)}>
                    <Form.Group controlId="firstName">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control 
                            type="text" 
                            placeholder="Enter first name"
                            value={firstName} 
                            onChange={e => setFirstName(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="lastName">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control 
                            type="text" 
                            placeholder="Enter last name"
                            value={lastName} 
                            onChange={e => setLastName(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="userEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control 
                            type="email" 
                            placeholder="Enter email"
                            value={email} 
                            onChange={e => setEmail(e.target.value)}
                            required
                        />
                        
                    </Form.Group>

                    <Form.Group controlId="mobileNo">
                        <Form.Label>Mobile Number</Form.Label>
                        <Form.Control 
                            type="text" 
                            placeholder="Enter Mobile Number"
                            value={mobileNo} 
                            onChange={e => setMobileNo(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="password1">
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                            type="password" 
                            placeholder="Password"
                            value={password1} 
                            onChange={e => setPassword1(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="password2">
                        <Form.Label>Verify Password</Form.Label>
                        <Form.Control 
                            type="password" 
                            placeholder="Verify Password"
                            value={password2} 
                            onChange={e => setPassword2(e.target.value)}
                            required
                        />
                    </Form.Group>

                    { isActive ? 
                        <Button className="mt-3" variant="primary" type="submit" id="submitBtn">
                            Register
                        </Button>
                        : 
                        <Button className="mt-3" variant="danger" type="submit" id="submitBtn" disabled>
                            Register
                        </Button>
                    }
                </Form>
                </div>


                <div className="d-flex flex-column m-5 w-50">

                <h2 className="my-4" id="register-welcome">Welcome to zine.repo</h2>
                <p className="my-2" id="register-text">Join our vibrant community of indie artists and explore a world of zines, books, and artistic expressions curated to celebrate creativity, individuality, and the vibrant spirit.</p>

                <p className="my-3" id="register-subtext">
                     Embrace the unique, the authentic, and the creative.
                </p>
                </div> 


            </Col >
                


            
           {/* <Col xs={12} md={12} lg={6}>
                <img
                    className="d-flex register-img"
                    src="https://purveyr.com/wp-content/uploads/2020/11/Purveyr-Fake-Zine-107-uai-2064x1376.jpg"
                    alt="First slide"
                />

            </Col>*/}
        </Row>
        </div>

    </>



    )
    
}