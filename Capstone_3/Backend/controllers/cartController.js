const Cart = require('../models/cart');
var ObjectId = require('mongoose').Types.ObjectId; //For mongoose
const Product = require('../models/product')

// #############################################################################

// Add product to cart
exports.addToCart = async (reqParams, userAuth) => {
  const productIdNew = reqParams;
  const userIdNew = userAuth.id;
console.log (productIdNew)
console.log (userAuth.isAdmin)

  if (!userAuth.isAdmin) {
    console.log (productIdNew)
      try {
        // Find the user's cart or create a new one if it doesn't exist
        let cart = await Cart.findOne({userId: userIdNew});

         console.log(`This is the cart variable : ${cart}`)
         
         let product = await Product.findById(productIdNew);
          console.log(`This is the product info : ${product}`)
       

        if (!cart) {
          cart = new Cart( {userId:userIdNew , products: [], total: product.price} );
          cart.products.push({ productId: productIdNew, quantity: 1, subtotal: product.price });
          console.log(`This is the NEW CART : ${cart}`)

        }else {

          let existingProductIndex = cart.products.findIndex(
          (product) => product.productId === productIdNew);



          if (existingProductIndex !== -1) {
              cart.products[existingProductIndex].quantity++;
               cart.products[existingProductIndex].subtotal = cart.products[existingProductIndex].quantity * product.price

            }else{
              cart.products.push({ productId: productIdNew, quantity: 1, subtotal: product.price });
            }
        } 


        let newTotal = 0

        for (let i = 0; i < cart.products.length; i++) {
          
           newTotal+= cart.products[i].subtotal
         
         } 

         cart.total = newTotal;




        // Save the updated cart
        await cart.save();
        console.log(`This is the cart variable : ${cart}`)

        return product


      } catch (error) {
        return ({ error: 'An error occurred while adding the product to the cart.' });
      }
  }

  let message = Promise.resolve("You don't have the access rights to do this action.");

    return message.then((value) => {
      return value
    })
};


// #############################################################################
// Get cart items for a user
exports.getCartItems = async (userAuth) => {
  const userIdNew = userAuth.id;
  console.log(userIdNew)


  if (!userAuth.isAdmin) {
      try {
        const cart = await Cart.findOne({userId: userIdNew})
         console.log(cart.products)
        if (!cart) {
          return false;
        }

        return(cart.products);
      } catch (error) {
        return ({ error: 'An error occurred while fetching the cart items.' });
      }
  }

   let message = Promise.resolve("You don't have the access rights to do this action.");

    return message.then((value) => {
      return value
    })

};

// #############################################################################


exports.updateCartItem = async (req, reqParams, userAuth) => {
  const quantity= req.body.quantity;
  const productId = reqParams;
  let userIdNew = userAuth.id;

  

  try {
    // Find the user's cart
    const cart = await Cart.findOne({ userId: userIdNew });

    if (!cart) {
      return ({ error: 'Cart not found' });
    }

    // Find the index of the product in the cart
    const productIndex = cart.products.findIndex(
      (product) => product.productId.toString() === productId
    );

    

    if (productIndex === -1) {
      return ({ error: 'Product not found in cart' });
    } else{

    // Update the quantity of the product
    cart.products[productIndex].quantity = quantity;
    

    let product = await Product.findById(productId);

    cart.products[productIndex].subtotal =
      cart.products[productIndex].quantity * product.price;
      
      }

     // Calculate the total of the cart
    let newTotal = 0

        for (let i = 0; i < cart.products.length; i++) {
          
           newTotal+= cart.products[i].subtotal
         
         } 

         cart.total = newTotal;

    // Save the updated cart
    await cart.save();

    return true
  } catch (error) {
    console.error(error);
    return ({ error: 'An error occurred while updating the cart' });
  }
};







// #############################################################################
// Remove product from cart
exports.removeFromCart = async (reqParams, userAuth) => {
  const productId = reqParams;
  const userIdNew = userAuth.id;

  if (!userAuth.isAdmin) {
    try {
      const cart = await Cart.findOne({ userId: userIdNew });


      if (!cart) {
        return ({ error: 'Cart not found.' });
      }

      cart.products = cart.products.filter(
        (product) => product.productId.toString() !== productId
      );

      console.log(cart.products)


      // Recalculate the total
      let total = 0;
      for (const product of cart.products) {
        total += product.subtotal;
      }

      // Update the total in the cart
      cart.total = total;

      console.log `This is the NEW CART TOTAL ${cart.total}`





      // Save the updated cart
      await cart.save();

      



      return cart;
    } catch (error) {
      return ({ error: 'An error occurred while removing the product from the cart.' });
    }
  }

   let message = Promise.resolve("You don't have the access rights to do this action.");

    return message.then((value) => {
      return value
    })  
};


// #############################################################################
// Clear cart for a user
exports.clearCart = async (reqParams, res, userAuth) => {
  const userIdNew = reqParams;


  if (!userAuth.isAdmin) {
      try {
        const cart = await Cart.findOne({ userId: userIdNew });
        if (!cart) {
          return false
        }

        // Clear the cart
        cart.products = [];

        // Save the updated cart
        await cart.save();

        return true
      } catch (error) {
        return ({ error: 'An error occurred while clearing the cart.' });
      }
  }

  let message = Promise.resolve("You don't have the access rights to do this action.");

    return message.then((value) => {
      return value
    })
};

// #############################################################################


// Get cart items for a user
exports.getUserCart = async (userAuth) => {
  const userIdNew = userAuth.id;
  console.log(userIdNew)


  if (!userAuth.isAdmin) {
      try {
        const cart = await Cart.findOne({userId: userIdNew})
         console.log(cart.products)
        if (!cart) {
          return ({ error: 'Cart not found.' });
        }

        return(cart);
      } catch (error) {
        return ({ error: 'An error occurred while fetching the cart items.' });
      }
  }

   let message = Promise.resolve("You don't have the access rights to do this action.");

    return message.then((value) => {
      return value
    })

};