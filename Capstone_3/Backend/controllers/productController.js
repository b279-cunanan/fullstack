const Product = require("../models/product");

// #############################################################################


// Create a new product 
exports.createProduct = async (data) => {
  const { name, description, price, category, image } = data.product; 
  console.log(data.product)
  console.log(data.isAdmin)

  if (data.isAdmin) {
      try {
        const newProduct = new Product({ name, description, price, category, image });
        const savedProduct = await newProduct.save();
        return savedProduct;
      } catch (error) {
        return {error: 'An error occurred while creating the product.'};
      }
  }

  let message = Promise.resolve("You don't have the access rights to do this action.");

    return message.then((value) => {
      return value
    })


};

// #############################################################################

// Get all products
exports.getAllProducts = async (req, res) => {
  try {
    const products = await Product.find();
    return(products);
  } catch (error) {
    return {error: 'An error occurred while fetching the product.'};
  }
};

// #############################################################################


// Get all "ACTIVE" products
module.exports.getAllActive = () => {
  return Product.find({isActive : true}).then(result => {
    
      return result;
  })
}

// #############################################################################

// Retrieve specific product
exports.getProduct = async (reqParams) => {
  const productId = reqParams;
  
  try {
    const product = await Product.findById(productId);
    if (!product) {
      return {error: 'Product not found.'};
    }
    return(product);
  } catch (error) {
    return {error: 'An error occurred while fetching the product.'};
  }
};

// #############################################################################

// Update a product
exports.updateProduct = async (reqParams, reqBody, isAdmin) => {
  const productId = reqParams;
  const { name, description, price, category } = reqBody;
  
      try {

        // Admin check
        if(isAdmin){
          const updatedProduct = await Product.findByIdAndUpdate(
            productId,
            { name, description, price, category },
            { new: true }
          );
          return updatedProduct;
        }

        let message = Promise.resolve("You don't have the access rights to do this action.");

        return message.then((value) => {
          return value
        })
        
      
        // Error messages
        if (!updatedProduct) {
          return {error: 'Product not found.'};
        }
        return(updatedProduct);
      } catch (error) {
        return {error: 'An error occurred while updating the product.'};
      }
  


};

// #############################################################################

// Delete a product
// exports.deleteProduct = async (reqParams, isAdmin) => {
//   const productId = reqParams;
//   console.log(productId)
//   if(isAdmin){
//       try {

//         // Admin check
//         if(isAdmin){
//         const deletedProduct = await Product.findByIdAndDelete(productId);
//         return true

//          // Error messages
//         if (!deletedProduct) {
//           return false;
//         }
        
//       } catch (error) {
//         return false;
//       }
//   }

//   let message = Promise.resolve("You don't have the access rights to do this action.");

//   return message.then((value) => {
//     return value
//   })


// };

// #############################################################################
// Archive product

module.exports.archiveProduct = (reqParams, isAdmin) => {
  console.log(isAdmin);

  if(isAdmin){
    let updatedProduct = {
    isActive : "false"
    }

    return Product.findByIdAndUpdate(reqParams, updatedProduct).then((product, error) => {
      if(error){
        return false;
      }else{
        return true 
          
      }
    })
  }

  let message = Promise.resolve("You don't have the access rights to do this action.");

  return message.then((value) => {
    return value
  })

  
}


// #############################################################################
// ACTIVATE product

module.exports.activateProduct = (reqParams, isAdmin) => {
  console.log(isAdmin);

  if(isAdmin){
    let updatedProduct = {
    isActive : "true"
    }

    return Product.findByIdAndUpdate(reqParams, updatedProduct).then((product, error) => {
      
      if(error){
        return false;
      }else{
        
        return true
      }
    })
  }

  let message = Promise.resolve("You don't have the access rights to do this action.");

  return message.then((value) => {
    return value
  })

  
}

// #############################################################################

// // Retrieve ALL ORDERS 
// module.exports.getAllOrders = (isAdmin) => {
//   return Product.find().gt("__v", 0).then(result => {
    
//     if(isAdmin){
//       return result;
//     }

//     let message = Promise.resolve("You don't have the access rights to do this action.");

//     return message.then((value) => {
//       return value
//     });

//   })
// }
    
















































// module.exports.addCourse = (data) => {

// 	// User is an admin
// 	if (data.isAdmin) {

// 		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
// 		// Uses the information from the request body to provide all the necessary information
// 		let newCourse = new Course({
// 			name : data.course.name,
// 			description : data.course.description,
// 			price : data.course.price
// 		});

// 		// Saves the created object to our database
// 		return newCourse.save().then((course, error) => {

// 			// Course creation successful
// 			if (error) {

// 				return false;

// 			// Course creation failed
// 			} else {

// 				return true;

// 			};

// 		});

// 	// User is not an admin
// 	} else {
// 		return false;
// 	};
	

// };

// // Retrieve all courses
// /*
// 	Steps:
// 	1. Retrieve all the courses from the database
// */
// module.exports.getAllCourses = () => {

// 	return Course.find({}).then(result => {

// 		return result;

// 	});

// };

// // Retrieve all ACTIVE courses
// /*
// 	Steps:
// 	1. Retrieve all the courses from the database with the property of "isActive" to true
// */
// module.exports.getAllActive = () => {

// 	return Course.find({isActive : true}).then(result => {
// 		return result;
// 	});

// };

// // Retrieving a specific course
// /*
// 	Steps:
// 	1. Retrieve the course that matches the course ID provided from the URL
// */
// module.exports.getCourse = (reqParams) => {

// 	return Course.findById(reqParams.courseId).then(result => {
// 		return result;
// 	});

// };

// // Update a course
// /*
// 	Steps:
// 	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
// 	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
// */
// // Information to update a course will be coming from both the URL parameters and the request body
// module.exports.updateCourse = (reqParams, reqBody) => {

// 	// Specify the fields/properties of the document to be updated
// 	let updatedCourse = {
// 		name : reqBody.name,
// 		description	: reqBody.description,
// 		price : reqBody.price
// 	};

// 	// Syntax
// 		// findByIdAndUpdate(document ID, updatesToBeApplied)
// 	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {

// 		// Course not updated
// 		if (error) {

// 			return false;

// 		// Course updated successfully
// 		} else {

// 			return true;
// 		};

// 	});

// };

// // Archive a course
// // In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
// // The "soft delete" happens here by simply updating the course "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active courses are retrieved
// // This allows us access to these records for future use and hides them away from users in our frontend application
// // There are instances where hard deleting records is required to maintain the records and clean our databases
// // The use of "hard delete" refers to removing records from our database permanently
// module.exports.archiveCourse = (reqParams) => {

// 	let updateActiveField = {
// 		isActive : false
// 	};

// 	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {

// 		// Course not archived
// 		if (error) {

// 			return false;

// 		// Course archived successfully
// 		} else {

// 			return true;

// 		}

// 	});
// };

// // Capstone 3 archive course
// // module.exports.archiveCourse = (reqParams, reqBody) => {

// // 	let updateActiveField = {
// // 		isActive : reqBody.isActive
// // 	};

// // 	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {

// // 		// Course not archived
// // 		if (error) {

// // 			return false;

// // 		// Course archived successfully
// // 		} else {

// // 			return true;

// // 		}

// // 	});
// // };