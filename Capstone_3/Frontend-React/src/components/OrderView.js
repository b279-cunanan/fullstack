import {useState, useEffect, useContext} from "react"
import {Container, Card, Button, Row, Col, Form} from "react-bootstrap"
import { useParams } from "react-router-dom"
import { Link } from "react-router-dom";
import UserContext from "../UserContext";

import OrderProdCard from "./OrderProdCard";

import Swal from "sweetalert2"

import List from '@mui/material/List';
import Divider from '@mui/material/Divider';










export default function ProductView(){

	const { user } = useContext(UserContext);

	// module that allows us to retrieve the courseId passed via URL
	// const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [image, setImage] = useState(0);
	const [category, setCategory] = useState(0);
	const [rating, setRating] = useState(0);
	const [shippingAddress, setShippingAddress] = useState("");
	const [contactNumber, setContactNumber] = useState("");

	const [allOrderProduct, setAllOrderProduct] = useState([])


	 const [productId, setProdId] = useState("");

	const [order, setOrder] = useState("");

	const [total, setTotal] = useState("");



	


// 	useEffect(() => {

// 	const token = localStorage.getItem('token');
  //     fetch(`http://localhost:4000/cart`, {
  //         method: "GET",
  //         headers: {
  //             "Content-Type": "application/json",
  //             Authorization: `Bearer ${localStorage.getItem('token')}`
  //         }
  //     })
  //         .then(res => res.json())
  //         .then(data => {

  //             console.log(data);
  //             setProdId(data.productId) 

  //         });
  // })




// 	useEffect(() => {
// 		console.log(productId);

// 		fetch(`http://localhost:4000/product/search/${productId}`)
// 		.then(res => res.json())
// 		.then(data => {
// 			console.log(data);

// 			setName(data.name);
// 			setDescription(data.description);
// 			setPrice(data.price);
// 			setImage(data.image);
// 			setCategory(data.category);
// 			setRating(data.rating);
			
// 		})

// 	}, [productId])



const token = localStorage.getItem('token');

	     
// *******GET TOTAL AMOUNT**************

	useEffect(() => {
	  

	  fetch(`http://localhost:4000/cart`, {
	    method: "POST",
	    headers: {
	      Authorization: `Bearer ${token}`,
	    },
	  })
	    .then((res) => res.json())
	    .then((data) => {
	      
	      console.log(data);

	     setTotal(data.total)
	    })
	    
	}, []);





	   //***********GET CART USER********** 

	useEffect(() => {
	  

	  fetch(`http://localhost:4000/cart`, {
	    method: "GET",
	    headers: {
	      Authorization: `Bearer ${token}`
	    }
	  })
	    .then(res => res.json())
	    .then(data => {
	      console.log(`this is the User cart: ${data}`);
	      console.log(data);

	      // Fetch the product details for each order
	      Promise.all(data.map(order => (
	        fetch(`http://localhost:4000/product/search/${order.productId}`)
	          .then(res => res.json())
	          .then(prod => ({

	            _id: prod._id,
	            productId: order.productId,
	            image: prod.image,
	            name: prod.name,
	            price: prod.price,
	            category: prod.category,
	            quantity: order.quantity,
	            subtotal: order.subtotal

	          }))
	      )))
	        .then(products => {
	          setProdId(products.prodId)
	          const cartProdCards = products.map(product => (
	            <Col key={product._id} className="m-0 ">
	              <OrderProdCard orderProp={product} />
	            </Col>
	          ));

	          setAllOrderProduct(cartProdCards);
	        })
	        .catch(error => {
	          console.error("Error fetching product details:", error);
	        });
	    });
	}, []);








	const checkOut = async () => {
	const token = localStorage.getItem('token');

	console.log (`THIS IS THE sHIPPING Address ${shippingAddress}`);
	console.log (`THIS IS THE CONTACT NUMBER ${contactNumber}`);	

	await fetch(`http://localhost:4000/order`, {
			method: "POST",
			headers: {
				"Content-type" : "application/json",
				Authorization : `Bearer ${token}`
			},
			body: JSON.stringify({
                        shippingAddress: shippingAddress,
                        contactNumber: contactNumber	
            })
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

		    setOrder(data);
		    console.log("NEW ORDER:", data); 

		    // If no user info is found, the "access" property will not be available
		    if(typeof data !== "undefined"){
		        

		        Swal.fire({
		          icon: 'success',
		          title: 'ORDER PLACED',
		          text: 'Thank you for placing your order with our online store!',
		          
		        })

		    }else{
		        Swal.fire({
		          icon: 'error',
		          title: 'Authentication Failed!',
		          text: 'Please try again!',
		        })
		    }
		         })
		         
		};

			//***********CART CONTENTS (start)**************** 

			// useEffect(() => {
			//   const token = localStorage.getItem("token");

			//   fetch(`http://localhost:4000/cart`, {
			//     method: "GET",
			//     headers: {
			//       Authorization: `Bearer ${token}`,
			//     },
			//   })
			//     .then((res) => res.json())
			//     .then((data) => {
			//       console.log(`this is the User cart: ${data}`);
			//       console.log(data);

			//       // Fetch the product details for each order
			//       Promise.all(
			//         data.map((order) =>
			//           fetch(`http://localhost:4000/product/search/${order.productId}`)
			//             .then((res) => res.json())
			//             .then((prod) => ({
			//               _id: prod._id,
			//               productId: order.productId,
			//               image: prod.image,
			//               name: prod.name,
			//               price: prod.price,
			//               category: prod.category,
			//               quantity: order.quantity,
			//               subtotal: order.subtotal,
			//             }))
			//         )
			//       )
			//         .then((products) => {
			//         	console.log(products)
			//           setCartItems(products);

			//         })
			//         .catch((error) => {
			//           console.error("Error fetching product details:", error);
			//         });
			//     })
			//     .catch((error) => {
			//       console.error("Error fetching cart items:", error);
			//     });
			// }, []);
			

			//***********CART CONTENTS (end)****************




		useEffect(() => {
		  const token = localStorage.getItem('token');

		  fetch(`http://localhost:4000/cart`, {
		    method: "GET",
		    headers: {
		      Authorization: `Bearer ${token}`
		    }
		  })
		    .then(res => res.json())
		    .then(data => {
		      console.log(`this is the User cart: ${data}`);
		      console.log(data);

		      // Fetch the product details for each order
		      Promise.all(data.map(order => (
		        fetch(`http://localhost:4000/product/search/${order.productId}`)
		          .then(res => res.json())
		          .then(prod => ({

		            _id: prod._id,
		            productId: order.productId,
		            image: prod.image,
		            name: prod.name,
		            price: prod.price,
		            category: prod.category,
		            quantity: order.quantity,
		            subtotal: order.subtotal

		          }))
		      )))
		        .then(products => {
		          setProdId(products.prodId)
		          const cartProdCards = products.map(product => (
		            <Col key={product._id} className="m-0">
		              <OrderProdCard orderProp={product} />
		            </Col>
		          ));

		          setAllOrderProduct(cartProdCards);
		        })
		        .catch(error => {
		          console.error("Error fetching product details:", error);
		        });
		    });
		}, []);
























// ******************RATING FUNCTION***************** 

	const roundedRating = Math.round(rating * 2) / 2;

	  // Generate an array of star icons based on the rating
	  const starIcons = [];
	  for (let i = 0; i < 5; i++) {
	    if (roundedRating >= i + 1) {
	      starIcons.push(<i key={i} className="fas fa-star"></i>);
	    } else if (roundedRating >= i + 0.5) {
	      starIcons.push(<i key={i} className="fas fa-star-half-alt"></i>);
	    } else {
	      starIcons.push(<i key={i} className="far fa-star"></i>);
	    }
	  } 



// *************ORDER SUMMARY TABLE******************

	  

	 
	  


	return(
		<>

		<h1 className="m-5"> Product Checkout </h1>
		<Container className=" px-5 mx-5" id="orderview-containter">
			<Row id="orderview-form">
			
				<Col className=" d-flex flex-column justify-content-center align-items-center">
					<Row className="my-2  w-100  ">
						<h4 className="pt-5 px-5 mb-5">Delivery Information</h4>
						<Form className="pb-5 px-5" id="orderview-form-text">
						     <Row className="mb-3">
						       <Form.Group as={Col} controlId="formGridEmail">
						         <Form.Label>Full Name</Form.Label>
						         <Form.Control  placeholder="Enter Full Name" />
						       </Form.Group>

						       <Form.Group as={Col} controlId="formGridPassword">
						         <Form.Label>Mobile Number</Form.Label>
						         <Form.Control
						                   placeholder="Please enter your mobile number"
						                   value={contactNumber}
						                   onChange={(e) => setContactNumber(e.target.value)}
						                   required
						                 />
						       </Form.Group>
						     </Row>

						     <Form.Group className="mb-3" controlId="formGridAddress1">
						       <Form.Label>Address</Form.Label>
						       <Form.Control 
						       				placeholder="Please enter your complete address"
						       				value={shippingAddress}
						                   	onChange={(e) => setShippingAddress(e.target.value)} 
						                   	required
						                   	/>
						     </Form.Group>

						     <Form.Group className="mb-3" controlId="formGridAddress2">
						       <Form.Label>Other Notes</Form.Label>
						       <Form.Control placeholder="Please enter your notes" />
						     </Form.Group>

						     
						   </Form>
					</Row>


					
					<hr className="w-100 "/>

					<div className=" p-5 my-3" id="orderview-products">
						<h4 className="mb-5"> Products
						</h4>



						<div style={{ width: '500px', overflowX: 'hidden' }}>

							<Row className="d-flex flex-column flex-nowrap" style={{maxHeight: 'calc(100vh - 400px)'}}>

								{allOrderProduct}
							</Row>
						</div>


					</div>

					<div className="d-flex justify-content-between py-3 mb-5 w-100" id="orderview-products-btn">
						<Link className=" p-4 text-warning" to={`/`} id="cont-shop-btn">Return to home</Link>

						<Link className="btn btn-warning p-4 text-light" to={`/products`} id="cont-shop-btn">Continue Shopping</Link>
					</div>


				</Col>

				<Col className="d-flex flex-column justify-content-flex-start align-items-center  p-5 my-3">
					<div className="w-100 d-flex justify-content-center align-items-center">

							
							{

								(user.isAdmin === true) ?

								<Link className="btn btn-danger w-100" to={`/products`}>Go back</Link>

								:

								<Link 
									className="btn btn-warning w-100 " 
									to={`/products`} 
									onClick={() => checkOut()}>

									PLACE ORDER NOW

								</Link>	
							}
					</div>

					<div className="w-100">

					<div className="max-width-400 mx-auto mt-4">
					      <h2>Order Summary</h2>
					      <table className="table">
					        <thead>
					          <tr>
					            <th>Item</th>
					            <th className="text-right">Amount</th>
					          </tr>
					        </thead>
					        <tbody>
					          <tr className="border-bot-none">
					            <td>Subtotal</td>
					            <td className="text-right">₱ {total}</td>
					          </tr>
					          <tr >
					            <td>Shipping Fee</td>
					            <td className="text-right">₱ 0</td>
					          </tr>
					          <tr className="border-bot-none text-bold">
					            <td>Total</td>
					            <td className="text-right">₱ {total}</td>
					          </tr>
					        </tbody>
					      </table>
					    </div>


					</div>

							
						
				</Col>
			</Row>

			
		</Container>

		</>
		);
}
