import { useState, useEffect, useContext } from "react";
import {Container, Card, Button, Row, Col, Form} from "react-bootstrap";
import { useParams, useNavigate, Link, Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import HomeBanner from "../components/HomeBanner";
import HotDeals from "../components/HotDeals";
import UserContext from "../UserContext";
// import logo from "../images/logo.jpeg"




export default function ProductView({productProp}){

	const data = {
		title: "YOU ARE ABOUT TO MAKE A TRANSACTION",
		content: "Buy products using your E-wallet. Less hassle, more efficient!",
		destination: "/products",
		label: "Other Products",
		
	}

	 // const { _id, name, description, price, image, category, rating } = productProp

	const { user } = useContext(UserContext);


	const { productId } = useParams();

	const navigate = useNavigate();


  
	const [name, setName] = useState('');
    const [category, setCategory] = useState('');
	const [description, setDescription] = useState('');
    // const [stocks, setStocks] = useState('');
    const [image, setImage] = useState('');
	const [price, setPrice] = useState('');
	const [quantity, setQuantity] = useState(1);
	const [total, setTotal] = useState('');

	useEffect(()=>{
		console.log(productId);

		fetch(`http://localhost:4000/cart`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

            
			setName(data.name);
            setCategory(data.category);
            setImage(data.image);
			setDescription(data.description);
			setPrice(data.price);

		});

		fetchCart();

	}, [productId])



	const fetchCart = async () => {

	        // Process a fetch request to the corresponding API


	        fetch(`http://localhost:4000/cart`, {
	            method: "GET",
	            headers: {
	            	"Content-Type": "application/json",
	                Authorization: `Bearer ${localStorage.getItem('token')}`
	            }
	        })
	        .then(res => res.json())
	        .then(data => {
	            console.log(data);

	            // If no user info is found, the "access" property will not be available
	            if(typeof data.access !== "undefined"){
	                localStorage.setItem("token", data.access);
	                

	                setTotal(data.total)

	            }else{
	                return false
	            }
	        })
	      };

console.log`This is the TOTAL ${total}`
	

	const buy = (productId) => {
        fetch(`http://localhost:4000/cart/${productId}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                        quantity: quantity	
            })
        })
            .then(res => res.json())
            .then(data => {

                console.log(data);

                if (data) {
                    Swal.fire({
                        title: "PRODUCT SOLD!",
                        icon: "success",
                        text: `Thank you for purchasing ${name}.`
                    })
                    navigate("/products");
                } else {
                    Swal.fire({
                        title: "Oops! Something went wrong",
                        icon: "error",
                        text: "Please try again."
                    })
                }
            });
    }


	return(
		
		<>
		
		{
				(user.isAdmin)
				?
					<Navigate to="/dashboard" />
				:
		<div className="p-5">
		<HomeBanner bannerProp={data}/>
		<div className="d-flex align-items-center justify-content-center flex-column">
			<Col className="my-2 " xs={12} md={12} lg={6}>
            <Card className="my-3 w-100  card-height shadow card-border shadow-md card-bg ">
            <Card.Img className='product-img-fit'
                src={image}
            />
                <Card.Header className='py-3 my-3 '>
                <Card.Title>
                    {name} 
                </Card.Title>
                </Card.Header>
                <Card.Body>
                <Card.Subtitle>
                    Description
                </Card.Subtitle>
                <Card.Text>
                    {description}
                </Card.Text>
                <Card.Subtitle>
                    Price
                </Card.Subtitle>
                <Card.Text>
                    Php {price}
                </Card.Text>
               {/* <Card.Subtitle>
                    Stocks
                </Card.Subtitle>
				
                <Card.Text>
                    {stocks} available
                </Card.Text>*/}
				<Card.Subtitle>
                    Quantity
                </Card.Subtitle>
				<Form.Control className="w-25 h-100 my-2"
                    type="number"
                    required
					value={quantity}
					onChange={(e) => setQuantity(e.target.value)}
                />
                </Card.Body>
			<div className="text-center">
            <Card.Footer>
			
            {
				
				(user._id !== null)
				?
					<Button id="checkOutBtn" className='w-50 btn-dark my-3 shadow' onClick={() => buy(productId)}>PLACE ORDER NOW</Button>
				:
					<Button className='w-50 btn-danger my-3 shadow' as={Link} to={`/login`}>LOGIN TO BUY</Button>
			}
            </Card.Footer>
			</div>
			

        </Card>
            </Col>
			</div>
		
		</div>
		}
		
		
		
		

		</>
	)
	
	}
