const mongoose = require('mongoose');

// Review schema
const reviewSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  product: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Product',
    required: true
  },
  rating: {
    type: Number,
    required: true,
    min: 1,
    max: 5
  },
  comment: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

// Review model
const Review = mongoose.model('Review', reviewSchema);



// Example usage
// mongoose.connect('mongodb://localhost/myapp', { useNewUrlParser: true, useUnifiedTopology: true })
//   .then(() => {
//     // Create a review instance
//     const review = new Review({
//       user: '6051a95e0830f93c53a354f1', // ID of the user from the User collection
//       product: '6051a95e0830f93c53a354f2', // ID of the product from the Product collection
//       rating: 4,
//       comment: 'Great product, highly recommended!'
//   });

//   // Save the review to the database
//   return review.save();
// })
// .then(savedReview => {
//   // Review successfully saved
//   console.log('Review saved:');
//   console.log(savedReview);
// })
// .catch(error => console.error(error))
// .finally(() => mongoose.disconnect());
