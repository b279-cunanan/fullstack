// Import the necessary modules and controllers
const express = require('express');
const router = express.Router();
const reviewController = require('../controllers/reviewController');
const auth = require("../auth.js");

// #############################################################################

// Create a review
router.post('/createReview', auth.verify,(req, res) => {
  let userAuth = auth.decode(req.headers.authorization);
  const reviewData = req.body;
  reviewController.createReview(reviewData, userAuth)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.status(500).send(error));
});

// #############################################################################

// Retrieve all reviews
router.get('/', (req, res) => {
  reviewController.getAllReviews()
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.status(500).send(error));
});

// #############################################################################

// Retrieve a specific review
router.get('/:reviewId', (req, res) => {
  const reviewId = req.params.reviewId;
  reviewController.getReviewById(reviewId)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.status(500).send(error));
});

// #############################################################################

// Update a specific review
router.put('/:reviewId', auth.verify, (req, res) => {
  let userAuth = auth.decode(req.headers.authorization);
  const reviewId = req.params.reviewId;
  const reviewData = req.body;
  reviewController.updateReview(reviewId, reviewData, userAuth)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.status(500).send(error));
});

// #############################################################################


// GET /products/:productId/reviews - Get reviews for a product
router.get('/products/:productId', (req, res) => {
  reviewController.getReviewsForProduct(req, res)
  .then(resultFromController => res.send(resultFromController))
  .catch(error => res.status(500).send(error));
});

// #############################################################################


// DELETE /reviews/:reviewId - Delete a specific review
router.delete('/reviews/:reviewId', auth.verify, (req, res) => {
  
  const reviewId = req.params.reviewId;
  reviewController.deleteReview(reviewId)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.status(500).send(error));
});

module.exports = router;
