const express = require('express');
const router = express.Router();
const auth = require("../auth.js");

// Import the Order controller
const orderController = require('../controllers/orderController'); 


// ############################################################################# 

// POST /orders - Create a new order
router.post('/', auth.verify, (req, res) => {
	let userAuth = auth.decode(req.headers.authorization);
	orderController.checkoutOrder(req.body, userAuth).then(resultFromController => res.send(resultFromController));
});


// ############################################################################# 

// GET /orders/:orderId - Retrieve a specific order
router.get('/:orderId', auth.verify, (req, res) => {
	let userAuth = auth.decode(req.headers.authorization);
	orderController.getOrderById(req, res, userAuth).then(resultFromController => res.send(resultFromController));
});


// ############################################################################# 

// GET /orders - Retrieve all orders for the authenticated user
router.get('/', auth.verify, (req, res) => {
	let userAuth = auth.decode(req.headers.authorization);
	orderController.getAllOrders(req, res, userAuth).then(resultFromController => res.send(resultFromController));
});


// ############################################################################# 

// PATCH /orders/:orderId - Update a specific order
router.patch('/:orderId', auth.verify, (req, res) => {
	let userAuth = auth.decode(req.headers.authorization);
	orderController.updateOrder(req, res, req.body, userAuth).then(resultFromController => res.send(resultFromController));
});


// ############################################################################# 

// DELETE /orders/:orderId - Delete a specific order
router.delete('/:orderId', auth.verify, (req, res) => {
	let userAuth = auth.decode(req.headers.authorization);
	orderController.deleteOrder(req, res, userAuth).then(resultFromController => res.send(resultFromController));
});

module.exports = router;
