const express = require('express');
const router = express.Router();

// Import the Category controller
const categoryController = require('../controllers/categoryController');

// #############################################################################

// POST /categories - Create a new category
router.post('/', (req, res) => {
  categoryController.createCategory(req, res)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.status(500).send(error.message));
});

// #############################################################################

// GET / - Retrieve all categories
router.get('/', (req, res) => {
  categoryController.getAllCategories(req, res)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.status(500).send(error.message));
});

// #############################################################################

// Get products by category
router.get('/:categoryId', (req, res) => {
  categoryController.getProductsByCategory(req, res)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.status(500).send(error.message));
});

// #############################################################################

// PUT /:categoryId - Update a specific category by ID
router.put('/:categoryId', (req, res) => {
  categoryController.updateCategory(req, res)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.status(500).send(error.message));
});

// #############################################################################

// DELETE /:categoryId - Delete a specific category by ID
router.delete('/:categoryId', (req, res) => {
  categoryController.deleteCategory(req, res)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.status(500).send(error.message));
});

module.exports = router;
