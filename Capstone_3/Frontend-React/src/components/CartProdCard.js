import { Card, Button, Form } from "react-bootstrap";
// In React.js we have 3 hooks

/*
1. useState
2. useEffect
3. useContext
*/

import {useState} from "react";
import { Link } from "react-router-dom"
import "./components.css";
import Swal from "sweetalert2";



export default function CartProdCard({orderProp, onUpdate, onRemove}){
  // checks if props was successfully passed
  console.log(orderProp);
  // checks the type of the passed data
  console.log(typeof orderProp);


// Destructuring the courseProp into their own variables
  // const { _id, name, description, price, image, category, rating } = productOrderProp

  const { _id, productId, image, name, price, category, quantity, subtotal } = orderProp

 


    console.log({productId});

const [allOrder, setAllOrder] = useState([])

const [newQuantity, setNewQuantity] = useState(quantity)

const updateQuantity = (productId) => {
      fetch(`http://localhost:4000/cart/${productId}`, {
          method: "PUT",
          headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${localStorage.getItem('token')}`
          },
          body: JSON.stringify({
                      quantity: newQuantity  
          })
      })
          .then(res => res.json())
          .then(data => {

              console.log(data);

          });
  }





  






  return(
   
   

            <Card className="my-5  d-flex flex-row align-items-center justify-content-center " id="cart-product-cards">

              

              <Card.Img variant="top" src={image} id="cart-image-product" className=" card-image "/>
                    <Card.Body className="d-flex flex-column mt-3 justify-content-space-between">

                    

                      <Card.Title className="text-dark text-center mt-0 mb-2" id="product-title">{name}</Card.Title>

                      <Card.Text className="text-dark my-0 prod-deet text-center " id="cart-product-category">
                        {category}
                      </Card.Text>

                      <div className="d-flex flex-column justify-content-center align-items-center">
                      <Card.Text className="text-dark text-center my-0 prod-deet">
                        ₱{price}
                      </Card.Text>
                      

                      {/*<Card.Text className=" text-dark text-center my-1 prod-deet d-flex align-items-center justify-content-center" id="category-text">
                        <div className="w-auto " id="category-box">
                          {category}
                        </div>
                      </Card.Text>*/}

                              <Form.Control 
                                className="w-50 h-100 my-2 mx-0"
                                type="number"
                                required
                                value={newQuantity}
                                onChange={(e) => setNewQuantity(e.target.value)}
                                onBlur={() => updateQuantity(productId)}
                              />
                      
                      {/*<Card.Text className=" text-dark text-center my-0 mb-auto prod-deet">
                        Qty: {quantity} 
                      </Card.Text>*/}

                      {/*<Card.Text className=" text-dark text-center my-0 mb-auto prod-deet">
                        total: ₱{subtotal} 
                      </Card.Text>*/}

                      </div>

                      <Card.Text className="text-dark text-center">
                         <Button className="my-2" variant="danger" size="sm" onClick={() => onRemove(productId)}>Remove</Button>
                      </Card.Text>

                    </Card.Body>
              
            </Card>

          
    )
}

