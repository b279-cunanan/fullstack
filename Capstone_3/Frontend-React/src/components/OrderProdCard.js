import { Card, Button, Form } from "react-bootstrap";
import {Row, Col} from "react-bootstrap";
// In React.js we have 3 hooks

/*
1. useState
2. useEffect
3. useContext
*/

import {useState} from "react";
import { Link } from "react-router-dom"
import "./components.css";
import Swal from "sweetalert2";



export default function CartProdCard({orderProp, onUpdate, onRemove}){
  // checks if props was successfully passed
  console.log(orderProp);
  // checks the type of the passed data
  console.log(typeof orderProp);


// Destructuring the courseProp into their own variables
  // const { _id, name, description, price, image, category, rating } = productOrderProp

  const { _id, productId, image, name, price, category, quantity, subtotal } = orderProp

 


    console.log({productId});

const [allOrder, setAllOrder] = useState([])

const [newQuantity, setNewQuantity] = useState(quantity)

const updateQuantity = (productId) => {
      fetch(`http://localhost:4000/cart/${productId}`, {
          method: "PUT",
          headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${localStorage.getItem('token')}`
          },
          body: JSON.stringify({
                      quantity: newQuantity  
          })
      })
          .then(res => res.json())
          .then(data => {

              console.log(data);

          });
  }





  






  return(
   
   

            <Card className="m-0  p-0 d-flex flex-row" id="order-product-cards">

            	

	            

	            	


		            <Card.Img variant="top" src={image} id="order-image-product" className=" card-image "/>
	              	
	              	



	              	

	              	
	                    <Card.Body className=" d-flex flex-column justify-content-center ">

		            <div className=" ps-5 ">      

		                    <Card.Title className="text-dark ml-3 mt-0 mb-2" id="product-title">{name}
		                    </Card.Title>

		                    <Card.Text className="text-dark ml-3 my-0  prod-deet" id="order-product-category">
		                        {category}
		                    </Card.Text>

		            </div>

	                     </Card.Body>

	            


	            <div>
	                

	                     
	               
	                     <Card.Body className=" d-flex flex-column  justify-content-center">
	                      <Card.Text className="text-dark text-center my-0 prod-deet">
	                        ₱{price}
	                      </Card.Text>

	                      <Card.Text className="text-dark text-center my-0 prod-deet">
	                        Qty:{quantity}
	                      </Card.Text>
	                      

	                      {/*<Card.Text className=" text-dark text-center my-1 prod-deet d-flex align-items-center justify-content-center" id="category-text">
	                        <div className="w-auto " id="category-box">
	                          {category}
	                        </div>
	                      </Card.Text>*/}

	                              
	                      
	                      {/*<Card.Text className=" text-dark text-center my-0 mb-auto prod-deet">
	                        Qty: {quantity} 
	                      </Card.Text>*/}

	                      {/*<Card.Text className=" text-dark text-center my-0 mb-auto prod-deet">
	                        total: ₱{subtotal} 
	                      </Card.Text>*/}
	                      </Card.Body>

	            </div>

	              

	            

              

             

                     

                    
              
            </Card>

          
    )
}

